<?php

use Symfony\Component\Config\Loader\LoaderInterface,
    Symfony\Component\HttpKernel\Kernel;

/**
 * AppKernel class.
 *
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class AppKernel extends Kernel
{

    // ~ Main methods.

    /**
     * @return array
     */
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Haru\CountryBundle\HaruCountryBundle(),
            new Haru\CityBundle\HaruCityBundle(),
            new Haru\ProvinceBundle\HaruProvinceBundle(),
            new Haru\RoleBundle\HaruRoleBundle(),
            new Haru\UserBundle\HaruUserBundle(),
            new Haru\CalculatorBundle\HaruCalculatorBundle(),
            new ContactBundle\ContactBundle(),
            new DashboardBundle\DashboardBundle(),
            new IndexBundle\IndexBundle(),
            new NumberBundle\NumberBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new ProviderBundle\ProviderBundle(),
            new RatingBundle\RatingBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    // ~

    /**
     * @param LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }

}
