<?php

namespace DashboardBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * DashboardBundle class.
 *
 * @package DashboardBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class DashboardBundle extends Bundle
{

}
