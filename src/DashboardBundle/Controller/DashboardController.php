<?php

namespace DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Response;

/**
 * DashboardController class.
 *
 * @package DashboardBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class DashboardController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to dashboard list action. */
        return $this->redirectToRoute('dashboard_dashboard_dashboard', array());
    }

    /**
     * @return Response
     */
    public function dashboardAction()
    {
        /* Prepare render data. */

        $renderData = array();

        // ~

        return $this->render('DashboardBundle:Dashboard:dashboard.html.twig', $renderData);;
    }

}
