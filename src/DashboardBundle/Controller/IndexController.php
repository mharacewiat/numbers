<?php

namespace DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * IndexController class.
 *
 * @package DashboardBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to dashboard index action. */
        return $this->redirectToRoute('dashboard_dashboard_index', array());
    }

}
