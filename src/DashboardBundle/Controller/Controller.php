<?php

namespace DashboardBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController;

/**
 * Controller class.
 *
 * @package DashboardBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

}
