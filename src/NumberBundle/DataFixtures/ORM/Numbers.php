<?php

namespace NumberBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\Common\DataFixture,
    NumberBundle\Entity\Number,
    NumberBundle\Service\Manager as NumberManager;

/**
 * Numbers class.
 *
 * @package NumberBundle\DataFixtures
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Numbers extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load numbers data. */
        $numbersData = $this->loadNumbersData();

        // ~

        /* Load numbers. */
        $this->loadNumbers($numbersData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $numbersData
     * @param ObjectManager $manager
     */
    protected function loadNumbers(array $numbersData, ObjectManager $manager)
    {
        /* Iterate through numbers data. */
        foreach ($numbersData as $key => $value) {
            /* Load number. */
            $this->loadNumber($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $numberData
     * @param ObjectManager $manager
     * @return Number
     */
    protected function loadNumber($key, array $numberData, ObjectManager $manager)
    {
        /* Build number. */
        $number = $this->buildNumber($key, $numberData, $manager);

        // ~

        return $number;
    }

    // ~

    /**
     * @param $key
     * @param array $numberData
     * @param ObjectManager $manager
     * @return Number
     */
    protected function buildNumber($key, array $numberData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $numberManager = $this->getNumberManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $number
         */
        list($number) = $this->prepareData($numberData);

        // ~

        $numberEntity = $numberManager->create();

        // ~

        $numberEntity->setNumber($number);

        // ~

        $numberManager->update($numberEntity, false);

        // ~

        /* Store number. */
        $this->storeNumber($key, $numberEntity);

        // ~

        return $number;
    }

    // ~

    /**
     * @param $key
     * @param Number $number
     * @return $this
     */
    protected function storeNumber($key, Number $number)
    {
        $this->setReference(vsprintf('number-%1$s', array($key)), $number);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadNumbersData()
    {
        return $this->loadData('numbers');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('number'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return NumberManager
     */
    protected function getNumberManager()
    {
        return $this->getContainer()->get('number.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 201;
    }

}
