<?php

namespace NumberBundle\EventListener;

use NumberBundle\Event\FilterNumberResponseEventInterface,
    NumberBundle\Event\FormEventInterface,
    NumberBundle\Event\GetNumberResponseEventInterface;

/**
 * NumberListener class.
 *
 * @package NumberBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class NumberListener extends NumberListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetNumberResponseEventInterface $event
     */
    public function onListInitialize(GetNumberResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    public function onListCompleted(FilterNumberResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetNumberResponseEventInterface $event
     */
    public function onEditInitialize(GetNumberResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    public function onEditCompleted(FilterNumberResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'number.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetNumberResponseEventInterface $event
     */
    public function onDeleteInitialize(GetNumberResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterNumberResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'number.delete');

        // ~

        return;
    }

}
