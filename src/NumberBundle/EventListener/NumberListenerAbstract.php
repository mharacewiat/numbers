<?php

namespace NumberBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    NumberBundle\Event\FilterNumberResponseEventInterface,
    NumberBundle\Event\FormEventInterface,
    NumberBundle\Event\GetNumberResponseEventInterface;

/**
 * NumberListenerAbstract abstract class.
 *
 * @package NumberBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class NumberListenerAbstract extends EventListener implements NumberListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetNumberResponseEventInterface $event
     */
    abstract public function onListInitialize(GetNumberResponseEventInterface $event);

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterNumberResponseEventInterface $event);

    // ~

    /**
     * @param GetNumberResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetNumberResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterNumberResponseEventInterface $event);

    // ~

    /**
     * @param GetNumberResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetNumberResponseEventInterface $event);

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterNumberResponseEventInterface $event);

}
