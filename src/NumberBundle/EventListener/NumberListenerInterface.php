<?php

namespace NumberBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    NumberBundle\Event\FilterNumberResponseEventInterface,
    NumberBundle\Event\FormEventInterface,
    NumberBundle\Event\GetNumberResponseEventInterface;

/**
 * NumberListenerInterface interface.
 *
 * @package NumberBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface NumberListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetNumberResponseEventInterface $event
     */
    public function onListInitialize(GetNumberResponseEventInterface $event);

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    public function onListCompleted(FilterNumberResponseEventInterface $event);

    // ~

    /**
     * @param GetNumberResponseEventInterface $event
     */
    public function onEditInitialize(GetNumberResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    public function onEditCompleted(FilterNumberResponseEventInterface $event);

    // ~

    /**
     * @param GetNumberResponseEventInterface $event
     */
    public function onDeleteInitialize(GetNumberResponseEventInterface $event);

    /**
     * @param FilterNumberResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterNumberResponseEventInterface $event);

}
