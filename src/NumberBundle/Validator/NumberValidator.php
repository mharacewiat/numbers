<?php

namespace NumberBundle\Validator;

use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

/**
 * NumberValidator class.
 *
 * @package NumberBundle\Validator
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class NumberValidator extends ConstraintValidator
{

    // ~ Main methods.

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        try {

        } catch (\Exception $exception) {
            /* Do nothing. */
        }

        // ~

        return;
    }

}
