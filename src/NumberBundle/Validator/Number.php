<?php

namespace NumberBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Number class.
 *
 * @package NumberBundle\Validator
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Number extends Constraint
{

}
