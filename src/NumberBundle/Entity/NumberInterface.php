<?php

namespace NumberBundle\Entity;

use Haru\ORM\Mapping\EntityInterface;

/**
 * NumberInterface interface.
 *
 * @package NumberBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface NumberInterface extends EntityInterface
{

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set number
     *
     * @param string $number
     *
     * @return NumberInterface
     */
    public function setNumber($number);

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return NumberInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return NumberInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

}
