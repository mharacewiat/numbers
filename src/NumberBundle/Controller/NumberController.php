<?php

namespace NumberBundle\Controller;

use Doctrine\DBAL\DBALException,
    NumberBundle\Entity\Number as NumberEntity,
    NumberBundle\Event\FilterNumberResponseEvent,
    NumberBundle\Event\FilterNumberResponseEventInterface,
    NumberBundle\Event\FormEvent,
    NumberBundle\Event\FormEventInterface,
    NumberBundle\Event\GetNumberResponseEvent,
    NumberBundle\Form\NumberType,
    NumberBundle\Repository\NumberRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * NumberController class.
 *
 * @package NumberBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class NumberController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to number list action. */
        return $this->redirectToRoute('number_number_list', array());
    }

    /**
     * @param $number
     * @param Request $request
     * @return Response
     */
    public function numberAction($number, Request $request)
    {
        $renderData = array(
            'number' => null,
            'form' => null,
        );

        // ~

        $numberResolver = $this->getNumberResolver();

        // ~

        try {
            /* Resolve. */
            $number = $numberResolver->resolve($number);
            //dump($number);
            //exit;
        } catch (\Exception $exception) {
            dump($exception);
            exit;
        }

        // ~

        return $this->render('NumberBundle:Number:number.html.twig', $renderData);
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'numbers' => null,
        );

        // ~

        /* Prepare services. */

        $numberManager = $this->getNumberManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetNumberResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetNumberResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var NumberRepository $numberRepository */
        $numberRepository = $numberManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all numbers. */
        $numbers = $numberRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['numbers'] = $numbers;

        // ~

        $response = $this->render('NumberBundle:Number:list.html.twig', $renderData);

        // ~

        $event = new FilterNumberResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterNumberResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'number' => null,
        );

        // ~

        /* Prepare services. */

        $numberManager = $this->getNumberManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var NumberRepository $numberRepository */
        $numberRepository = $numberManager->getRepository();

        /** @var NumberEntity $number */
        $number = $numberRepository->find($id);

        // ~

        if (true == is_null($number)) {
            /** @var Number $number */
            $number = $numberManager->create();
        }

        // ~

        $event = new GetNumberResponseEvent($number, $request);
        $eventDispatcher->dispatch(GetNumberResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new NumberType();
        $formOptions = array();

        $form = $this->createForm($formType, $number, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update number. */
                    $numberManager->update($number, true);

                    // ~

                    $event = new FilterNumberResponseEvent($number, $response);
                    $eventDispatcher->dispatch(FilterNumberResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('number_number_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['number'] = $number;

        // ~

        return $this->render('NumberBundle:Number:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $numberManager = $this->getNumberManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var NumberRepository $numberRepository */
        $numberRepository = $numberManager->getRepository();

        /** @var NumberEntity $number */
        $number = $numberRepository->find($id);

        // ~

        $event = new GetNumberResponseEvent($number, $request);
        $eventDispatcher->dispatch(GetNumberResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($number)) {
            try {
                /* Update number. */
                $numberManager->delete($number, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('number_number_list', array());

        // ~

        $event = new FilterNumberResponseEvent($number, $response);
        $eventDispatcher->dispatch(FilterNumberResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
