<?php

namespace NumberBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    NumberBundle\Service\Manager as NumberManager,
    NumberBundle\Service\Resolver as NumberResolver;

/**
 * Controller class.
 *
 * @package NumberBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return NumberManager
     */
    protected function getNumberManager()
    {
        return $this->getContainer()->get('number.manager');
    }

    // ~

    /**
     * @return NumberResolver
     */
    protected function getNumberResolver()
    {
        return $this->getContainer()->get('number.resolver');
    }

}
