<?php

namespace NumberBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * NumberBundle class.
 *
 * @package NumberBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class NumberBundle extends Bundle
{

}
