<?php

namespace NumberBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * NumberRepository class.
 *
 * @package NumberBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class NumberRepository extends EntityRepository
{

}
