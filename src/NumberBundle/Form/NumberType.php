<?php

namespace NumberBundle\Form;

use Haru\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormEvent,
    Symfony\Component\Form\FormEvents,
    Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * NumberType class.
 *
 * @package NumberBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class NumberType extends AbstractType
{

    // ~ Main methods.

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* Build parent form. */
        parent::buildForm($builder, $options);

        // ~

        /* Number. */
        $builder->add('number', 'text', array(
            'label' => 'number.number.number',
        ));

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param FormBuilderInterface $builder
     */
    protected function registerListeners(FormBuilderInterface $builder)
    {
        /* Register parent listeners. */
        parent::registerListeners($builder);

        // ~

        /* Register created at event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'createdAtListener'), 0);

        /* Register deleted at event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'deletedAtListener'), 0);

        // ~

        return;
    }

    // ~

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /* Configure parent options. */
        parent::configureOptions($resolver);

        // ~

        $resolver->setDefaults(array(
            'data_class' => 'NumberBundle\\Entity\\Number',
            // ~
            'created_at' => false,
            'deleted_at' => false,
            // ~
            'submit' => true,
        ));

        // ~

        $resolver->addAllowedTypes('created_at', array(
            'bool',
            'int',
        ));

        $resolver->addAllowedTypes('deleted_at', array(
            'bool',
            'int',
        ));

        // ~

        return;
    }

    // ~ Events.

    /**
     * @param FormEvent $event
     */
    public function createdAtListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $createdAt */
        $createdAt = $form->getConfig()->getOption('created_at');

        // ~

        if (true == $createdAt) {
            /* Created at. */
            $form->add('created_at', 'datetime', array(
                'label' => 'number.number.created_at',
            ));
        }

        // ~

        return;
    }

    /**
     * @param FormEvent $event
     */
    public function deletedAtListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $deletedAt */
        $deletedAt = $form->getConfig()->getOption('deleted_at');

        // ~

        if (true == $deletedAt) {
            /* Deleted at. */
            $form->add('deleted_at', 'datetime', array(
                'label' => 'number.number.deleted_at',
            ));
        }

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getName()
    {
        return 'number_number';
    }

}
