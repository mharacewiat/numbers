<?php

namespace NumberBundle\Event;

/**
 * FilterNumberResponseEventInterface interface.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterNumberResponseEventInterface extends NumberEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'number.number.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'number.number.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'number.number.delete.completed';

}
