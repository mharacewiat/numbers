<?php

namespace NumberBundle\Event;

use Haru\Component\EventDispatcher\FormEventInterface as BaseFormEventInterface;

/**
 * FormEventInterface interface.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FormEventInterface extends BaseFormEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const EDIT_SUCCESS = 'number.number.edit.success';

}
