<?php

namespace NumberBundle\Event;

use NumberBundle\Entity\NumberInterface,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GetNumberResponseEventAbstract abstract class.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GetNumberResponseEventAbstract extends NumberEvent implements GetNumberResponseEventInterface
{

    // ~ Properties.

    /**
     * @var Response|null
     */
    protected $response = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    // ~ Magic methods.

    /**
     * GetNumberResponseEventAbstract constructor.
     *
     * @param NumberInterface|null $number
     * @param Request|null $request
     */
    public function __construct(NumberInterface $number = null, Request $request = null)
    {
        /* Construct parent. */
        parent::__construct($number);

        // ~

        $this->setRequest($request);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     * @return $this
     */
    protected function setResponse(Response $response = null)
    {
        $this->response = $response;
        return $this;
    }

    // ~

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     * @return $this
     */
    protected function setRequest(Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

}
