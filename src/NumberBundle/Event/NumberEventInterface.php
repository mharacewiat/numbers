<?php

namespace NumberBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    NumberBundle\Entity\NumberInterface;

/**
 * NumberEventInterface interface.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface NumberEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return NumberInterface|null
     */
    public function getNumber();

}
