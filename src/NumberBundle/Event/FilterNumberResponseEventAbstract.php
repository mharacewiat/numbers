<?php

namespace NumberBundle\Event;

/**
 * FilterNumberResponseEventAbstract abstract class.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterNumberResponseEventAbstract extends NumberEventAbstract implements FilterNumberResponseEventInterface
{

}
