<?php

namespace NumberBundle\Event;

use Haru\Component\EventDispatcher\Event,
    NumberBundle\Entity\NumberInterface;

/**
 * NumberEventAbstract abstract class.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class NumberEventAbstract extends Event implements NumberEventInterface
{

    // ~ Properties.

    /**
     * @var NumberInterface|null
     */
    protected $number = null;

    // ~ Magic methods.

    /**
     * NumberEventAbstract constructor.
     *
     * @param NumberInterface|null $number
     */
    public function __construct(NumberInterface $number = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setNumber($number);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return NumberInterface|null
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param NumberInterface|null $number
     * @return $this
     */
    protected function setNumber(NumberInterface $number = null)
    {
        $this->number = $number;
        return $this;
    }

}
