<?php

namespace NumberBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetNumberResponseEventInterface interface.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetNumberResponseEventInterface extends NumberEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'number.number.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'number.number.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'number.number.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
