<?php

namespace NumberBundle\Event;

/**
 * FilterNumberResponseEvent class.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterNumberResponseEvent extends FilterNumberResponseEventAbstract
{

}
