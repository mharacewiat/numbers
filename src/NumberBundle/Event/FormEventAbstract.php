<?php

namespace NumberBundle\Event;

use FOS\UserBundle\Event\FormEvent as BaseFormEvent;

/**
 * FormEventAbstract abstract class.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FormEventAbstract extends BaseFormEvent implements FormEventInterface
{

}
