<?php

namespace NumberBundle\Event;

/**
 * GetNumberResponseEvent class.
 *
 * @package NumberBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetNumberResponseEvent extends GetNumberResponseEventAbstract
{

}
