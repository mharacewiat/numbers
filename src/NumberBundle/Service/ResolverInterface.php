<?php

namespace NumberBundle\Service;

/**
 * ResolverInterface interface.
 *
 * @package NumberBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ResolverInterface
{

    // ~ Main methods.

    /**
     * @param $number
     */
    public function resolve($number);

}
