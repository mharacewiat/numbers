<?php

namespace NumberBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    NumberBundle\Entity\NumberInterface;

/**
 * ManagerInterface interface.
 *
 * @package NumberBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return NumberInterface
     */
    public function create();

    /**
     * @param NumberInterface $number
     * @param bool $flush
     * @return $this
     */
    public function update(NumberInterface $number, $flush = true);

    /**
     * @param NumberInterface $number
     * @param bool $flush
     * @return $this
     */
    public function delete(NumberInterface $number, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getClass();

}
