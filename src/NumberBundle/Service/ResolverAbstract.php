<?php

namespace NumberBundle\Service;

/**
 * ResolverAbstract abstract class.
 *
 * @package NumberBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ResolverAbstract implements ResolverInterface
{

    // ~ Main methods.

    /**
     * @param $number
     * @return bool
     */
    public function resolve($number)
    {
        return true;
    }

}
