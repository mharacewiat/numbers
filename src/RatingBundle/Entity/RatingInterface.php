<?php

namespace RatingBundle\Entity;

use Haru\ORM\Mapping\EntityInterface;

/**
 * RatingInterface interface.
 *
 * @package RatingBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface RatingInterface extends EntityInterface
{

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return RatingInterface
     */
    public function setRate($rate);

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return RatingInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return RatingInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

}
