<?php

namespace RatingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * RatingBundle class.
 *
 * @package RatingBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RatingBundle extends Bundle
{

}
