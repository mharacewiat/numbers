<?php

namespace RatingBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * RatingRepository class.
 *
 * @package RatingBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RatingRepository extends EntityRepository
{

}
