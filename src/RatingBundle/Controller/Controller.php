<?php

namespace RatingBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    RatingBundle\Service\Manager as RatingManager;

/**
 * Controller class.
 *
 * @package RatingBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return RatingManager
     */
    protected function getRatingManager()
    {
        return $this->getContainer()->get('rating.manager');
    }

}
