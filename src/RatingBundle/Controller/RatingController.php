<?php

namespace RatingBundle\Controller;

use Doctrine\DBAL\DBALException,
    RatingBundle\Entity\Rating,
    RatingBundle\Event\FilterRatingResponseEvent,
    RatingBundle\Event\FilterRatingResponseEventInterface,
    RatingBundle\Event\FormEvent,
    RatingBundle\Event\FormEventInterface,
    RatingBundle\Event\GetRatingResponseEvent,
    RatingBundle\Form\RatingType,
    RatingBundle\Repository\RatingRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * RatingController class.
 *
 * @package RatingBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RatingController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to rating list action. */
        return $this->redirectToRoute('rating_rating_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'ratings' => null,
        );

        // ~

        /* Prepare services. */

        $ratingManager = $this->getRatingManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetRatingResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetRatingResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var RatingRepository $ratingRepository */
        $ratingRepository = $ratingManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all ratings. */
        $ratings = $ratingRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['ratings'] = $ratings;

        // ~

        $response = $this->render('RatingBundle:Rating:list.html.twig', $renderData);

        // ~

        $event = new FilterRatingResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterRatingResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'rating' => null,
        );

        // ~

        /* Prepare services. */

        $ratingManager = $this->getRatingManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var RatingRepository $ratingRepository */
        $ratingRepository = $ratingManager->getRepository();

        /** @var Rating $rating */
        $rating = $ratingRepository->find($id);

        // ~

        if (true == is_null($rating)) {
            /** @var Rating $rating */
            $rating = $ratingManager->create();
        }

        // ~

        $event = new GetRatingResponseEvent($rating, $request);
        $eventDispatcher->dispatch(GetRatingResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new RatingType();
        $formOptions = array();

        $form = $this->createForm($formType, $rating, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update rating. */
                    $ratingManager->update($rating, true);

                    // ~

                    $event = new FilterRatingResponseEvent($rating, $response);
                    $eventDispatcher->dispatch(FilterRatingResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('rating_rating_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['rating'] = $rating;

        // ~

        return $this->render('RatingBundle:Rating:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $ratingManager = $this->getRatingManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var RatingRepository $ratingRepository */
        $ratingRepository = $ratingManager->getRepository();

        /** @var Rating $rating */
        $rating = $ratingRepository->find($id);

        // ~

        $event = new GetRatingResponseEvent($rating, $request);
        $eventDispatcher->dispatch(GetRatingResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($rating)) {
            try {
                /* Update rating. */
                $ratingManager->delete($rating, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('rating_rating_list', array());

        // ~

        $event = new FilterRatingResponseEvent($rating, $response);
        $eventDispatcher->dispatch(FilterRatingResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
