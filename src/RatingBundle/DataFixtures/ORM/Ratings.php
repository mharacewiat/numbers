<?php

namespace RatingBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\Common\DataFixture,
    RatingBundle\Entity\Rating,
    RatingBundle\Service\Manager as RatingManager;

/**
 * Ratings class.
 *
 * @package RatingBundle\DataFixtures
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Ratings extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load ratings data. */
        $ratingsData = $this->loadRatingsData();

        // ~

        /* Load ratings. */
        $this->loadRatings($ratingsData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $ratingsData
     * @param ObjectManager $manager
     */
    protected function loadRatings(array $ratingsData, ObjectManager $manager)
    {
        /* Iterate through ratings data. */
        foreach ($ratingsData as $key => $value) {
            /* Load rating. */
            $this->loadRating($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $ratingData
     * @param ObjectManager $manager
     * @return Rating
     */
    protected function loadRating($key, array $ratingData, ObjectManager $manager)
    {
        /* Build rating. */
        $rating = $this->buildRating($key, $ratingData, $manager);

        // ~

        return $rating;
    }

    // ~

    /**
     * @param $key
     * @param array $ratingData
     * @param ObjectManager $manager
     * @return Rating
     */
    protected function buildRating($key, array $ratingData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $ratingManager = $this->getRatingManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $rate
         */
        list($rate) = $this->prepareData($ratingData);

        // ~

        $rating = $ratingManager->create();

        // ~

        $rating->setRate($rate);

        // ~

        $ratingManager->update($rating, false);

        // ~

        /* Store rating. */
        $this->storeRating($key, $rating);

        // ~

        return $rating;
    }

    // ~

    /**
     * @param $key
     * @param Rating $rating
     * @return $this
     */
    protected function storeRating($key, Rating $rating)
    {
        $this->setReference(vsprintf('rating-%1$s', array($key)), $rating);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadRatingsData()
    {
        return $this->loadData('ratings');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('rate'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return RatingManager
     */
    protected function getRatingManager()
    {
        return $this->getContainer()->get('rating.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 201;
    }

}
