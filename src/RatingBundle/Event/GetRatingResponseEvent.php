<?php

namespace RatingBundle\Event;

/**
 * GetRatingResponseEvent class.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetRatingResponseEvent extends GetRatingResponseEventAbstract
{

}
