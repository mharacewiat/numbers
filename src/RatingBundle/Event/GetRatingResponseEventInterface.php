<?php

namespace RatingBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetRatingResponseEventInterface interface.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetRatingResponseEventInterface extends RatingEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'rating.rating.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'rating.rating.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'rating.rating.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
