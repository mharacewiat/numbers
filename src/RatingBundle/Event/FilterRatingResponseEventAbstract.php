<?php

namespace RatingBundle\Event;

/**
 * FilterRatingResponseEventAbstract abstract class.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterRatingResponseEventAbstract extends RatingEventAbstract implements FilterRatingResponseEventInterface
{

}
