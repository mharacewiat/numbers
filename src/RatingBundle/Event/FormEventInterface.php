<?php

namespace RatingBundle\Event;

use Haru\Component\EventDispatcher\FormEventInterface as BaseFormEventInterface;

/**
 * FormEventInterface interface.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FormEventInterface extends BaseFormEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const EDIT_SUCCESS = 'rating.rating.edit.success';

}
