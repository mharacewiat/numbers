<?php

namespace RatingBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    RatingBundle\Entity\RatingInterface;

/**
 * RatingEventInterface interface.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface RatingEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return RatingInterface|null
     */
    public function getRating();

}
