<?php

namespace RatingBundle\Event;

/**
 * FilterRatingResponseEvent class.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterRatingResponseEvent extends FilterRatingResponseEventAbstract
{

}
