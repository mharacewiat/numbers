<?php

namespace RatingBundle\Event;

use Haru\Component\EventDispatcher\Event,
    RatingBundle\Entity\RatingInterface;

/**
 * RatingEventAbstract abstract class.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class RatingEventAbstract extends Event implements RatingEventInterface
{

    // ~ Properties.

    /**
     * @var RatingInterface|null
     */
    protected $rating = null;

    // ~ Magic methods.

    /**
     * RatingEventAbstract constructor.
     *
     * @param RatingInterface|null $rating
     */
    public function __construct(RatingInterface $rating = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setRating($rating);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return RatingInterface|null
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param RatingInterface|null $rating
     * @return $this
     */
    protected function setRating(RatingInterface $rating = null)
    {
        $this->rating = $rating;
        return $this;
    }

}
