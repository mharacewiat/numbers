<?php

namespace RatingBundle\Event;

use RatingBundle\Entity\RatingInterface,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GetRatingResponseEventAbstract abstract class.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GetRatingResponseEventAbstract extends RatingEvent implements GetRatingResponseEventInterface
{

    // ~ Properties.

    /**
     * @var Response|null
     */
    protected $response = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    // ~ Magic methods.

    /**
     * GetRatingResponseEventAbstract constructor.
     *
     * @param RatingInterface|null $rating
     * @param Request|null $request
     */
    public function __construct(RatingInterface $rating = null, Request $request = null)
    {
        /* Construct parent. */
        parent::__construct($rating);

        // ~

        $this->setRequest($request);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     * @return $this
     */
    protected function setResponse(Response $response = null)
    {
        $this->response = $response;
        return $this;
    }

    // ~

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     * @return $this
     */
    protected function setRequest(Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

}
