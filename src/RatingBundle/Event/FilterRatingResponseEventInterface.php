<?php

namespace RatingBundle\Event;

/**
 * FilterRatingResponseEventInterface interface.
 *
 * @package RatingBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterRatingResponseEventInterface extends RatingEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'rating.rating.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'rating.rating.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'rating.rating.delete.completed';

}
