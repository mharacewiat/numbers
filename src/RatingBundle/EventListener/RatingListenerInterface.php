<?php

namespace RatingBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    RatingBundle\Event\FilterRatingResponseEventInterface,
    RatingBundle\Event\FormEventInterface,
    RatingBundle\Event\GetRatingResponseEventInterface;

/**
 * RatingListenerInterface interface.
 *
 * @package RatingBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface RatingListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetRatingResponseEventInterface $event
     */
    public function onListInitialize(GetRatingResponseEventInterface $event);

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    public function onListCompleted(FilterRatingResponseEventInterface $event);

    // ~

    /**
     * @param GetRatingResponseEventInterface $event
     */
    public function onEditInitialize(GetRatingResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    public function onEditCompleted(FilterRatingResponseEventInterface $event);

    // ~

    /**
     * @param GetRatingResponseEventInterface $event
     */
    public function onDeleteInitialize(GetRatingResponseEventInterface $event);

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterRatingResponseEventInterface $event);

}
