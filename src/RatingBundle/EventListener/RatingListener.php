<?php

namespace RatingBundle\EventListener;

use RatingBundle\Event\FilterRatingResponseEventInterface,
    RatingBundle\Event\FormEventInterface,
    RatingBundle\Event\GetRatingResponseEventInterface;

/**
 * RatingListener class.
 *
 * @package RatingBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RatingListener extends RatingListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetRatingResponseEventInterface $event
     */
    public function onListInitialize(GetRatingResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    public function onListCompleted(FilterRatingResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetRatingResponseEventInterface $event
     */
    public function onEditInitialize(GetRatingResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    public function onEditCompleted(FilterRatingResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'rating.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetRatingResponseEventInterface $event
     */
    public function onDeleteInitialize(GetRatingResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterRatingResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'rating.delete');

        // ~

        return;
    }

}
