<?php

namespace RatingBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    RatingBundle\Event\FilterRatingResponseEventInterface,
    RatingBundle\Event\FormEventInterface,
    RatingBundle\Event\GetRatingResponseEventInterface;

/**
 * RatingListenerAbstract abstract class.
 *
 * @package RatingBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class RatingListenerAbstract extends EventListener implements RatingListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetRatingResponseEventInterface $event
     */
    abstract public function onListInitialize(GetRatingResponseEventInterface $event);

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterRatingResponseEventInterface $event);

    // ~

    /**
     * @param GetRatingResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetRatingResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterRatingResponseEventInterface $event);

    // ~

    /**
     * @param GetRatingResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetRatingResponseEventInterface $event);

    /**
     * @param FilterRatingResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterRatingResponseEventInterface $event);

}
