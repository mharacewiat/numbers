<?php

namespace RatingBundle\Service;

use Doctrine\Common\Persistence\ObjectManager,
    Doctrine\Common\Persistence\ObjectRepository,
    RatingBundle\Entity\RatingInterface;

/**
 * ManagerAbstract abstract class.
 *
 * @package RatingBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ManagerAbstract implements ManagerInterface
{

    // ~ Properties.

    /**
     * @var string
     */
    protected $class;

    // ~

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    // ~ Magic methods.

    /**
     * ManagerAbstract constructor.
     *
     * @param $class
     * @param ObjectManager $objectManager
     */
    public function __construct($class, ObjectManager $objectManager)
    {
        $this
            ->setClass($class)
            ->setObjectManager($objectManager);

        // ~

        return;
    }

    // ~ Main methods.

    /**
     * @return RatingInterface
     */
    public function create()
    {
        $reflectionClass = new \ReflectionClass($this->getClass());
        $instance = $reflectionClass->newInstanceArgs(func_get_args());

        // ~

        return $instance;
    }

    /**
     * @param RatingInterface $rating
     * @param bool $flush
     * @return $this
     */
    public function update(RatingInterface $rating, $flush = true)
    {
        $objectManager = $this->getObjectManager();

        // ~

        $objectManager->persist($rating);

        // ~

        if (true == $flush) {
            $objectManager->flush();
        }

        // ~

        return $this;
    }

    /**
     * @param RatingInterface $rating
     * @param bool $flush
     * @return $this
     */
    public function delete(RatingInterface $rating, $flush = true)
    {
        $objectManager = $this->getObjectManager();

        // ~

        $objectManager->remove($rating);

        // ~

        if (true == $flush) {
            $objectManager->flush();
        }

        // ~

        return $this;
    }

    // ~ Getters and setters.

    /**
     * Repository getter.
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        if (true == is_null($objectManager = $this->getObjectManager())) {
            throw new \InvalidArgumentException('Object manager does not exist');
        }

        // ~

        $class = $this->getClass();
        $repository = $objectManager->getRepository($class);

        // ~

        return $repository;
    }

    // ~

    /**
     * Class getter.
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Class setter.
     *
     * @param string $class
     * @return ManagerAbstract
     */
    protected function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    // ~

    /**
     * ObjectManager getter.
     *
     * @return ObjectManager
     */
    protected function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * ObjectManager setter.
     *
     * @param ObjectManager $objectManager
     * @return ManagerAbstract
     */
    protected function setObjectManager($objectManager)
    {
        $this->objectManager = $objectManager;
        return $this;
    }

}
