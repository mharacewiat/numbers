<?php

namespace RatingBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    RatingBundle\Entity\RatingInterface;

/**
 * ManagerInterface interface.
 *
 * @package RatingBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return RatingInterface
     */
    public function create();

    /**
     * @param RatingInterface $rating
     * @param bool $flush
     * @return $this
     */
    public function update(RatingInterface $rating, $flush = true);

    /**
     * @param RatingInterface $rating
     * @param bool $flush
     * @return $this
     */
    public function delete(RatingInterface $rating, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~

    /**
     * @return string
     */
    public function getClass();

}
