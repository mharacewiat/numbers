<?php

namespace ContactBundle\Form;

use FOS\UserBundle\Model\UserInterface;
use Haru\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormEvent,
    Symfony\Component\Form\FormEvents,
    Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * contactType class.
 *
 * @package ContactBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ContactType extends AbstractType
{

    // ~ Main methods.

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* Build parent form. */
        parent::buildForm($builder, $options);

        // ~

        /* Body. */
        $builder->add('body', 'textarea', array(
            'label' => 'contact.contact.body',
        ));

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param FormBuilderInterface $builder
     */
    protected function registerListeners(FormBuilderInterface $builder)
    {
        /* Register parent listeners. */
        parent::registerListeners($builder);

        // ~

        /* Register subject listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'subjectListener'), 0);

        // ~

        /* Register from listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'fromListener'), 0);

        // ~

        return;
    }

    // ~

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /* Configure parent options. */
        parent::configureOptions($resolver);

        // ~

        $resolver->setDefaults(array(
            'data_class' => null,
            // ~
            'subject' => null,
            'user' => null,
            // ~
            'submit' => true,
        ));

        // ~

        $resolver->addAllowedTypes('subject', array(
            'null',
            'string',
        ));

        $resolver->addAllowedTypes('user', array(
            'null',
            'FOS\\UserBundle\\Model\\UserInterface',
        ));

        // ~

        return;
    }

    // ~ Events.

    /**
     * @param FormEvent $event
     */
    public function subjectListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $subject */
        $subject = $form->getConfig()->getOption('subject');

        // ~

        $type = 'hidden';

        if (true == is_null($subject)) {
            $type = 'text';
        }

        // ~

        /* Subject. */
        $form->add('subject', $type, array(
            'label' => 'contact.contact.subject',
            'data' => $subject,
        ));

        // ~

        return;
    }

    /**
     * @param FormEvent $event
     */
    public function fromListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var null|UserInterface $user */
        $user = $form->getConfig()->getOption('user');

        // ~

        $data = null;

        if (false == is_null($user)) {
            $data = $user->getEmail();
        }

        // ~

        /* From. */
        $form->add('from', 'email', array(
            'label' => 'contact.contact.from',
            'required' => false,
            'data' => $data,
        ));

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getName()
    {
        return 'contact_contact';
    }

}
