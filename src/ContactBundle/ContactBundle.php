<?php

namespace ContactBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ContactBundle class.
 *
 * @package ContactBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ContactBundle extends Bundle
{

}
