<?php

namespace ContactBundle\Controller;

use ContactBundle\Form\ContactType,
    Symfony\Component\Form\FormInterface,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * ContactController class.
 *
 * @package ContactBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ContactController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to contact list action. */
        return $this->redirectToRoute('contact_contact_contact', array());
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function contactAction(Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
        );


        $formType = new ContactType();
        $formOptions = array(
            'subject' => null,
            'user' => $this->getUser(),
        );

        $form = $this->createForm($formType, null, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                try {
                    /* Send. */
                    $this->send($form);

                    // ~

                    $this->addFlash('success', 'contact.contact');

                    // ~

                    return $this->redirectToRoute('index_index_index', array());
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();

        // ~

        return $this->render('ContactBundle:Contact:contact.html.twig', $renderData);;
    }

    // ~ Helpers.

    /**
     * @param FormInterface $form
     * @return $this
     * @throws \Exception
     */
    protected function send(FormInterface $form)
    {
        $mailer = $this->getMailer();

        //~

        /* Build message. */
        $message = $this->buildMessage($form);

        // ~

        try {
            /* Send. */
            $count = $mailer->send($message, $failedRecipients);

            // ~

            if (
                ($count == 0) ||
                (count($failedRecipients) > 0)
            ) {
                throw new \Exception('Mailer failure');
            }
        } catch (\Exception $exception) {
            /* Rethrow exception. */
            throw $exception;
        }

        // ~

        return $this;
    }

    // ~

    /**
     * @param FormInterface $form
     * @return \Swift_Message
     */
    protected function buildMessage(FormInterface $form)
    {
        /* Get message data. */

        /**
         * @var string $from
         * @var string $subject
         * @var string $body
         * @var mixed $to
         * @var mixed $bcc
         */
        list(
            $from,
            $subject,
            $body,
            // ~
            $to,
            $bcc
            ) = $this->getMessageData($form);

        // ~

        $message = \Swift_Message::newInstance($subject, $body, 'text/html', 'utf-8');

        // ~

        if (false == is_null($from)) {
            $message->setFrom($from);
        }

        // ~

        if (false == is_null($to)) {
            $message->setTo($to);
        }

        // ~

        if (false == is_null($bcc)) {
            $message->setBcc($bcc);
        }

        // ~

        return $message;
    }

    // ~ Getters and setters.

    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getMessageData(FormInterface $form)
    {
        /**
         * @var string $from
         * @var string $subject
         * @var string $body
         */
        list(
            $from,
            $subject,
            $body
            ) = $this->getMessageFormData($form);

        // ~

        /**
         * @var string $from
         * @var string $subject
         * @var string $body
         */
        list(
            $to,
            $bcc
            ) = $this->getMessageContainerData();

        // ~

        $return = array(
            $from,
            $subject,
            $body,
            // ~
            $to,
            $bcc,
        );

        return $return;
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getMessageFormData(FormInterface $form)
    {
        $from = null;
        $subject = null;
        $body = null;

        // ~

        if (true == $form->has('from')) {
            $from = $form->get('from')->getData();
        }

        if (true == $form->has('subject')) {
            $subject = $form->get('subject')->getData();
        }

        if (true == $form->has('body')) {
            $body = $form->get('body')->getData();
        }

        // ~

        $return = array(
            $from,
            $subject,
            $body,
        );

        return $return;
    }

    /**
     * @return array
     */
    protected function getMessageContainerData()
    {
        $container = $this->getContainer();

        // ~

        $to = null;
        $bcc = null;

        // ~

        if (true == $container->hasParameter('contact.contact.message.to')) {
            $to = $container->getParameter('contact.contact.message.to');
        }

        if (true == $container->hasParameter('contact.contact.message.bcc')) {
            $bcc = $container->getParameter('contact.contact.message.bcc');
        }

        // ~

        $return = array(
            $to,
            $bcc,
        );

        return $return;
    }

}
