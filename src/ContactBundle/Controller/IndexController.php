<?php

namespace ContactBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * IndexController class.
 *
 * @package ContactBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to contact index action. */
        return $this->redirectToRoute('contact_contact_index', array());
    }

}
