<?php

namespace ContactBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController;

/**
 * Controller class.
 *
 * @package ContactBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return \Swift_Mailer
     */
    protected function getMailer()
    {
        return $this->getContainer()->get('mailer');
    }

}
