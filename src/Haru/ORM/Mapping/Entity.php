<?php

namespace Haru\ORM\Mapping;

/**
 * Entity abstract class.
 *
 * @package Haru\ORM\Mapping
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Entity implements EntityInterface
{

}
