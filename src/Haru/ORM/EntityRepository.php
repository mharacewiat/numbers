<?php

namespace Haru\ORM;

use Doctrine\ORM\EntityRepository as BaseEntityRepository,
    Doctrine\ORM\QueryBuilder,
    Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * EntityRepository abstract class.
 *
 * @package Haru\ORM
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class EntityRepository extends BaseEntityRepository
{

    // ~ Main methods.

    /**
     * @param array|null $conditions
     * @param null $offset
     * @param null $limit
     * @param bool $fetchJoinCollection
     * @return Paginator
     */
    public function getAll(array $conditions = null, $offset = null, $limit = null, $fetchJoinCollection = true)
    {
        /* Get query builder. */
        $queryBuilder = $this->getQueryBuilder();

        // ~

        /* Handle limit and offset. */

        if (false == is_null($limit)) {
            if (false == is_null($offset)) {
                $queryBuilder->setFirstResult($offset);
            }

            // ~

            $queryBuilder->setMaxResults($limit);
        }

        // ~

        $paginator = $this->paginateQueryBuilder($queryBuilder, $fetchJoinCollection);
        return $paginator;
    }

    // ~ Helpers.

    /**
     * @param QueryBuilder $queryBuilder
     * @param bool $fetchJoinCollection
     * @return Paginator
     */
    protected function paginateQueryBuilder(QueryBuilder $queryBuilder, $fetchJoinCollection = true)
    {
        $query = $queryBuilder->getQuery();
        $paginator = new Paginator($query, $fetchJoinCollection);

        // ~

        return $paginator;
    }

    // ~ Getters and setters.

    /**
     * @return QueryBuilder
     * @throws \Exception
     */
    public function getQueryBuilder()
    {
        /* Get alias. */
        $alias = $this->getAlias();

        // ~

        /* Build query builder. */
        $queryBuilder = $this->createQueryBuilder($alias);

        // ~

        return $queryBuilder;
    }

    // ~

    /**
     * @return string
     * @throws \Exception
     */
    protected function getAlias()
    {
        $reflectionClass = new \ReflectionClass($this->getClassName());

        // ~

        $className = $reflectionClass->getShortName();

        // ~

        /* Match all big letters. */
        $count = preg_match_all('/([A-Z])/', $className, $matches, 0);

        // ~

        if ($count > 0) {
            /** @var array $matches */
            list($matches,) = $matches;
            return strtolower(implode(null, $matches));
        }

        // ~

        throw new \Exception('Unable to get alias');
    }

}
