<?php

namespace Haru\ProvinceBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * ProvinceRepository class.
 *
 * @package Haru\ProvinceBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProvinceRepository extends EntityRepository
{

}
