<?php

namespace Haru\ProvinceBundle\Controller;

use Doctrine\DBAL\DBALException,
    Haru\ProvinceBundle\Entity\Province,
    Haru\ProvinceBundle\Event\FilterProvinceResponseEvent,
    Haru\ProvinceBundle\Event\FilterProvinceResponseEventInterface,
    Haru\ProvinceBundle\Event\FormEvent,
    Haru\ProvinceBundle\Event\FormEventInterface,
    Haru\ProvinceBundle\Event\GetProvinceResponseEvent,
    Haru\ProvinceBundle\Form\ProvinceType,
    Haru\ProvinceBundle\Repository\ProvinceRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * ProvinceController class.
 *
 * @package Haru\ProvinceBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProvinceController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to province list action. */
        return $this->redirectToRoute('haru_province_province_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'provinces' => null,
        );

        // ~

        /* Prepare services. */

        $provinceManager = $this->getProvinceManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetProvinceResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetProvinceResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var ProvinceRepository $provinceRepository */
        $provinceRepository = $provinceManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all provinces. */
        $provinces = $provinceRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['provinces'] = $provinces;

        // ~

        $response = $this->render('HaruProvinceBundle:Province:list.html.twig', $renderData);

        // ~

        $event = new FilterProvinceResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterProvinceResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'province' => null,
        );

        // ~

        /* Prepare services. */

        $provinceManager = $this->getProvinceManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var ProvinceRepository $provinceRepository */
        $provinceRepository = $provinceManager->getRepository();

        /** @var Province $province */
        $province = $provinceRepository->find($id);

        // ~

        if (true == is_null($province)) {
            /** @var Province $province */
            $province = $provinceManager->create();
        }

        // ~

        $event = new GetProvinceResponseEvent($province, $request);
        $eventDispatcher->dispatch(GetProvinceResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new ProvinceType();
        $formOptions = array();

        $form = $this->createForm($formType, $province, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update province. */
                    $provinceManager->update($province, true);

                    // ~


                    $event = new FilterProvinceResponseEvent($province, $response);
                    $eventDispatcher->dispatch(FilterProvinceResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('haru_province_province_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['province'] = $province;

        // ~

        return $this->render('HaruProvinceBundle:Province:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $provinceManager = $this->getProvinceManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var ProvinceRepository $provinceRepository */
        $provinceRepository = $provinceManager->getRepository();

        /** @var Province $province */
        $province = $provinceRepository->find($id);

        // ~

        $event = new GetProvinceResponseEvent($province, $request);
        $eventDispatcher->dispatch(GetProvinceResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($province)) {
            try {
                /* Update province. */
                $provinceManager->delete($province, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('haru_province_province_list', array());

        // ~

        $event = new FilterProvinceResponseEvent($province, $response);
        $eventDispatcher->dispatch(FilterProvinceResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
