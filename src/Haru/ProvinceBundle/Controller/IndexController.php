<?php

namespace Haru\ProvinceBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * IndexController class.
 *
 * @package Haru\ProvinceBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to province index action. */
        return $this->redirectToRoute('haru_province_province_index', array());
    }

}
