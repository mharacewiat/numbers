<?php

namespace Haru\ProvinceBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    Haru\ProvinceBundle\Service\Manager as ProvinceManager;

/**
 * Controller abstract class.
 *
 * @package Haru\ProvinceBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return ProvinceManager
     */
    protected function getProvinceManager()
    {
        return $this->getContainer()->get('haru_province.manager');
    }

}
