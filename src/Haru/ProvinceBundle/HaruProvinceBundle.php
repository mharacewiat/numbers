<?php

namespace Haru\ProvinceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HaruProvinceBundle class.
 *
 * @package Haru\ProvinceBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruProvinceBundle extends Bundle
{

}
