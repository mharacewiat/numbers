<?php

namespace Haru\ProvinceBundle\Entity;

use Doctrine\Common\Collections\Collection,
    Haru\CityBundle\Entity\CityInterface,
    Haru\CountryBundle\Entity\CountryInterface,
    Haru\ORM\Mapping\EntityInterface;

/**
 * ProvinceInterface interface.
 *
 * @package Haru\ProvinceBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ProvinceInterface extends EntityInterface
{

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     *
     * @return ProvinceInterface
     */
    public function setAbbreviation($abbreviation);

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProvinceInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProvinceInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProvinceInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

    // ~

    /**
     * Add city
     *
     * @param CityInterface $city
     *
     * @return ProvinceInterface
     */
    public function addCity(CityInterface $city);

    /**
     * Remove city
     *
     * @param CityInterface $city
     */
    public function removeCity(CityInterface $city);

    /**
     * Get cities
     *
     * @return Collection
     */
    public function getCities();

    // ~

    /**
     * Set country
     *
     * @param CountryInterface $country
     *
     * @return ProvinceInterface
     */
    public function setCountry(CountryInterface $country = null);

    /**
     * Get country
     *
     * @return CountryInterface
     */
    public function getCountry();

}
