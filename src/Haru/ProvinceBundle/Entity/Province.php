<?php

namespace Haru\ProvinceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection,
    Doctrine\Common\Collections\Collection,
    Haru\CityBundle\Entity\CityInterface,
    Haru\CountryBundle\Entity\CountryInterface,
    Haru\ORM\Mapping\Entity;

/**
 * Province class.
 *
 * @package Haru\ProvinceBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Province extends Entity implements ProvinceInterface
{

    // ~ Properties.

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $abbreviation;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $deleted_at;

    // ~

    /**
     * @var Collection
     */
    protected $cities;

    // ~

    /**
     * @var CountryInterface
     */
    protected $country;

    // ~ Magic methods.

    /**
     * ProvinceInterface constructor.
     */
    public function __construct()
    {
        $this->cities = new ArrayCollection();

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     *
     * @return ProvinceInterface
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProvinceInterface
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProvinceInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProvinceInterface
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    // ~

    /**
     * Add city
     *
     * @param CityInterface $city
     *
     * @return ProvinceInterface
     */
    public function addCity(CityInterface $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param CityInterface $city
     */
    public function removeCity(CityInterface $city)
    {
        $this->cities->removeElement($city);
    }

    /**
     * Get cities
     *
     * @return Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    // ~

    /**
     * Set country
     *
     * @param CountryInterface $country
     *
     * @return ProvinceInterface
     */
    public function setCountry(CountryInterface $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return CountryInterface
     */
    public function getCountry()
    {
        return $this->country;
    }

}
