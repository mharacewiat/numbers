<?php

namespace Haru\ProvinceBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator,
    Symfony\Component\DependencyInjection\ContainerBuilder,
    Symfony\Component\DependencyInjection\Loader,
    Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * HaruProvinceExtension class.
 *
 * @package Haru\ProvinceBundle\DependencyInjection
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruProvinceExtension extends Extension
{

    // ~ Main methods.

    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        // ~

        /* Load configuration. */
        $this->loadConfiguration($config, $container);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $config
     * @param ContainerBuilder $container
     * @return $this
     */
    protected function loadConfiguration(array $config, ContainerBuilder $container)
    {
        $container->setParameter('haru_province.class', $config['class']);

        // ~

        return $this;
    }

}
