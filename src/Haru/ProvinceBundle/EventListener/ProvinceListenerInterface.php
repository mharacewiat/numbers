<?php

namespace Haru\ProvinceBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    Haru\ProvinceBundle\Event\FilterProvinceResponseEventInterface,
    Haru\ProvinceBundle\Event\FormEventInterface,
    Haru\ProvinceBundle\Event\GetProvinceResponseEventInterface;

/**
 * ProvinceListenerInterface interface.
 *
 * @package Haru\ProvinceBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ProvinceListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    public function onListInitialize(GetProvinceResponseEventInterface $event);

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    public function onListCompleted(FilterProvinceResponseEventInterface $event);

    // ~

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    public function onEditInitialize(GetProvinceResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    public function onEditCompleted(FilterProvinceResponseEventInterface $event);

    // ~

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    public function onDeleteInitialize(GetProvinceResponseEventInterface $event);

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterProvinceResponseEventInterface $event);

}
