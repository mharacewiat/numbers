<?php

namespace Haru\ProvinceBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    Haru\ProvinceBundle\Event\FilterProvinceResponseEventInterface,
    Haru\ProvinceBundle\Event\FormEventInterface,
    Haru\ProvinceBundle\Event\GetProvinceResponseEventInterface;

/**
 * ProvinceListenerAbstract abstract class.
 *
 * @package Haru\ProvinceBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ProvinceListenerAbstract extends EventListener implements ProvinceListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    abstract public function onListInitialize(GetProvinceResponseEventInterface $event);

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterProvinceResponseEventInterface $event);

    // ~

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetProvinceResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterProvinceResponseEventInterface $event);

    // ~

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetProvinceResponseEventInterface $event);

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterProvinceResponseEventInterface $event);

}
