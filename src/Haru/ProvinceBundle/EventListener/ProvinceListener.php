<?php

namespace Haru\ProvinceBundle\EventListener;

use Haru\ProvinceBundle\Event\FilterProvinceResponseEventInterface,
    Haru\ProvinceBundle\Event\FormEventInterface,
    Haru\ProvinceBundle\Event\GetProvinceResponseEventInterface;

/**
 * ProvinceListener class.
 *
 * @package Haru\ProvinceBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProvinceListener extends ProvinceListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    public function onListInitialize(GetProvinceResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    public function onListCompleted(FilterProvinceResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    public function onEditInitialize(GetProvinceResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    public function onEditCompleted(FilterProvinceResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'province.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetProvinceResponseEventInterface $event
     */
    public function onDeleteInitialize(GetProvinceResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterProvinceResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterProvinceResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'province.delete');

        // ~

        return;
    }

}
