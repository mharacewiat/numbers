<?php

namespace Haru\ProvinceBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * ManagerInterface interface.
 *
 * @package Haru\ProvinceBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return ProvinceInterface
     */
    public function create();

    /**
     * @param ProvinceInterface $province
     * @param bool $flush
     * @return $this
     */
    public function update(ProvinceInterface $province, $flush = true);

    /**
     * @param ProvinceInterface $province
     * @param bool $flush
     * @return $this
     */
    public function delete(ProvinceInterface $province, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~

    /**
     * @return string
     */
    public function getClass();

}
