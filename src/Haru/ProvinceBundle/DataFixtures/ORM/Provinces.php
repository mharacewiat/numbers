<?php

namespace Haru\ProvinceBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\Common\DataFixture,
    Haru\ProvinceBundle\Entity\Province,
    Haru\ProvinceBundle\Service\Manager as ProvinceManager;

/**
 * Provinces class.
 *
 * @package Haru\ProvinceBundle\DataFixtures
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Provinces extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load provinces data. */
        $provincesData = $this->loadProvincesData();

        // ~

        /* Load provinces. */
        $this->loadProvinces($provincesData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $provincesData
     * @param ObjectManager $manager
     */
    protected function loadProvinces(array $provincesData, ObjectManager $manager)
    {
        /* Iterate through provinces data. */
        foreach ($provincesData as $key => $value) {
            /* Load province. */
            $this->loadProvince($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $provinceData
     * @param ObjectManager $manager
     * @return Province
     */
    protected function loadProvince($key, array $provinceData, ObjectManager $manager)
    {
        /* Build province. */
        $province = $this->buildProvince($key, $provinceData, $manager);

        // ~

        return $province;
    }

    // ~

    /**
     * @param $key
     * @param array $provinceData
     * @param ObjectManager $manager
     * @return Province
     */
    protected function buildProvince($key, array $provinceData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $provinceManager = $this->getProvinceManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $name
         * @var string $country
         */
        list($name, , $country) = $this->prepareData($provinceData);

        // ~

        /* Get clean country reference. */
        if (
            (0 === strpos($country, '@')) &&
            (true == $this->hasReference($countryKey = substr($country, 1)))
        ) {
            $country = $this->getReference($countryKey);
        } else {
            $country = null;
        }

        // ~

        $province = $provinceManager->create();

        // ~

        $province->setAbbreviation($key);
        $province->setName($name);
        $province->setCountry($country);

        // ~

        $provinceManager->update($province, false);

        // ~

        /* Store province. */
        $this->storeProvince($key, $province);

        // ~

        return $province;
    }

    // ~

    /**
     * @param $key
     * @param Province $province
     * @return $this
     */
    protected function storeProvince($key, Province $province)
    {
        $this->setReference(vsprintf('province-%1$s', array($key)), $province);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadProvincesData()
    {
        return $this->loadData('provinces');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('name', 'teryt', 'country'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return ProvinceManager
     */
    protected function getProvinceManager()
    {
        return $this->getContainer()->get('haru_province.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 201;
    }

}
