<?php

namespace Haru\ProvinceBundle\Event;

/**
 * FilterProvinceResponseEventAbstract abstract class.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterProvinceResponseEventAbstract extends ProvinceEventAbstract implements FilterProvinceResponseEventInterface
{

}
