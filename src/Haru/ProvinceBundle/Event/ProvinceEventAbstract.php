<?php

namespace Haru\ProvinceBundle\Event;

use Haru\Component\EventDispatcher\Event,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * ProvinceEventAbstract abstract class.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ProvinceEventAbstract extends Event implements ProvinceEventInterface
{

    // ~ Properties.

    /**
     * @var ProvinceInterface|null
     */
    protected $province = null;

    // ~ Magic methods.

    /**
     * ProvinceEventAbstract constructor.
     *
     * @param ProvinceInterface|null $province
     */
    public function __construct(ProvinceInterface $province = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setProvince($province);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return ProvinceInterface|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param ProvinceInterface|null $province
     * @return $this
     */
    protected function setProvince(ProvinceInterface $province = null)
    {
        $this->province = $province;
        return $this;
    }

}
