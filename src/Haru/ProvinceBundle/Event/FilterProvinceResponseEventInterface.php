<?php

namespace Haru\ProvinceBundle\Event;

/**
 * FilterProvinceResponseEventInterface interface.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterProvinceResponseEventInterface extends ProvinceEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'haru_province.province.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'haru_province.province.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'haru_province.province.delete.completed';

}
