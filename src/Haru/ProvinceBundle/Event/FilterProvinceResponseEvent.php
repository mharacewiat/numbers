<?php

namespace Haru\ProvinceBundle\Event;

/**
 * FilterProvinceResponseEvent class.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterProvinceResponseEvent extends FilterProvinceResponseEventAbstract
{

}
