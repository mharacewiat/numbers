<?php

namespace Haru\ProvinceBundle\Event;

use Haru\ProvinceBundle\Entity\ProvinceInterface,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GetProvinceResponseEventAbstract abstract class.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GetProvinceResponseEventAbstract extends ProvinceEvent implements GetProvinceResponseEventInterface
{

    // ~ Properties.

    /**
     * @var Response|null
     */
    protected $response = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    // ~ Magic methods.

    /**
     * GetProvinceResponseEventAbstract constructor.
     *
     * @param ProvinceInterface|null $province
     * @param Request|null $request
     */
    public function __construct(ProvinceInterface $province = null, Request $request = null)
    {
        /* Construct parent. */
        parent::__construct($province);

        // ~

        $this->setRequest($request);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     * @return $this
     */
    protected function setResponse(Response $response = null)
    {
        $this->response = $response;
        return $this;
    }

    // ~

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     * @return $this
     */
    protected function setRequest(Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

}
