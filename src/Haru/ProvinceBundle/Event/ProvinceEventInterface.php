<?php

namespace Haru\ProvinceBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * ProvinceEventInterface interface.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ProvinceEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return ProvinceInterface|null
     */
    public function getProvince();

}
