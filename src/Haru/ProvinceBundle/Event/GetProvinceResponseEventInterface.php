<?php

namespace Haru\ProvinceBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetProvinceResponseEventInterface interface.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetProvinceResponseEventInterface extends ProvinceEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'haru_province.province.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'haru_province.province.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'haru_province.province.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
