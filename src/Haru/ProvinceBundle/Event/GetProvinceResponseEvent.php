<?php

namespace Haru\ProvinceBundle\Event;

/**
 * GetProvinceResponseEvent class.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetProvinceResponseEvent extends GetProvinceResponseEventAbstract
{

}
