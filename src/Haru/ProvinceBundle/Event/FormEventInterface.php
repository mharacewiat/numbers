<?php

namespace Haru\ProvinceBundle\Event;

use Haru\Component\EventDispatcher\FormEventInterface as BaseFormEventInterface;

/**
 * FormEventInterface interface.
 *
 * @package Haru\ProvinceBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FormEventInterface extends BaseFormEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const EDIT_SUCCESS = 'haru_province.province.edit.success';

}
