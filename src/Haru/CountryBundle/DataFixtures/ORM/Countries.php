<?php

namespace Haru\CountryBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\Common\DataFixture,
    Haru\CountryBundle\Entity\Country,
    Haru\CountryBundle\Service\Manager as CountryManager;

/**
 * Countries class.
 *
 * @package Haru\CountryBundle\DataFixtures\ORM
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Countries extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load countries data. */
        $countriesData = $this->loadCountriesData();

        // ~

        /* Load countries. */
        $this->loadCountries($countriesData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $countriesData
     * @param ObjectManager $manager
     */
    protected function loadCountries(array $countriesData, ObjectManager $manager)
    {
        /* Iterate through countries data. */
        foreach ($countriesData as $key => $value) {
            /* Load country. */
            $this->loadCountry($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $countryData
     * @param ObjectManager $manager
     */
    protected function loadCountry($key, array $countryData, ObjectManager $manager)
    {
        /* Build country. */
        $countryEntity = $this->buildCountry($countryData, $manager);

        // ~

        /* Store country. */
        $this->storeCountry($key, $countryEntity);

        // ~

        return;
    }

    // ~

    /**
     * @param array $countryData
     * @param ObjectManager $manager
     * @return Country
     */
    protected function buildCountry(array $countryData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $countryManager = $this->getCountryManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $name
         * @var string $iso3166_2
         * @var string $iso3166_3
         */
        list(
            $name,
            $iso3166_2,
            $iso3166_3
            ) = $this->prepareData($countryData);

        unset($iso3166_2, $iso3166_3);

        // ~

        $country = $countryManager->create();

        // ~

        $country->setName($name);
        //$country->setIso31662($so3166_2);
        //$country->setIso31663($so3166_3);

        // ~

        $countryManager->update($country, false);

        // ~

        return $country;
    }

    // ~

    /**
     * @param $key
     * @param Country $country
     * @return $this
     */
    protected function storeCountry($key, Country $country)
    {
        $this->setReference(vsprintf('country-%1$s', array($key)), $country);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadCountriesData()
    {
        return $this->loadData('countries');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('name', 'so3166_2', 'so3166_3'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return CountryManager
     */
    protected function getCountryManager()
    {
        return $this->getContainer()->get('haru_country.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 101;
    }

}
