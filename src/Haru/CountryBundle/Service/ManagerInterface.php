<?php

namespace Haru\CountryBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    Haru\CountryBundle\Entity\CountryInterface;

/**
 * ManagerInterface interface.
 *
 * @package Haru\CountryBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return CountryInterface
     */
    public function create();

    /**
     * @param CountryInterface $country
     * @param bool $flush
     * @return $this
     */
    public function update(CountryInterface $country, $flush = true);

    /**
     * @param CountryInterface $country
     * @param bool $flush
     * @return $this
     */
    public function delete(CountryInterface $country, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~

    /**
     * @return string
     */
    public function getClass();

}
