<?php

namespace Haru\CountryBundle\EventListener;

use Haru\CountryBundle\Event\FilterCountryResponseEventInterface,
    Haru\CountryBundle\Event\FormEventInterface,
    Haru\CountryBundle\Event\GetCountryResponseEventInterface;

/**
 * CountryListener class.
 *
 * @package Haru\CountryBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CountryListener extends CountryListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetCountryResponseEventInterface $event
     */
    public function onListInitialize(GetCountryResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    public function onListCompleted(FilterCountryResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetCountryResponseEventInterface $event
     */
    public function onEditInitialize(GetCountryResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    public function onEditCompleted(FilterCountryResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'country.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetCountryResponseEventInterface $event
     */
    public function onDeleteInitialize(GetCountryResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterCountryResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'country.delete');

        // ~

        return;
    }

}
