<?php

namespace Haru\CountryBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    Haru\CountryBundle\Event\FilterCountryResponseEventInterface,
    Haru\CountryBundle\Event\FormEventInterface,
    Haru\CountryBundle\Event\GetCountryResponseEventInterface;

/**
 * CountryListenerAbstract abstract class.
 *
 * @package Haru\CountryBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class CountryListenerAbstract extends EventListener implements CountryListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetCountryResponseEventInterface $event
     */
    abstract public function onListInitialize(GetCountryResponseEventInterface $event);

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterCountryResponseEventInterface $event);

    // ~

    /**
     * @param GetCountryResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetCountryResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterCountryResponseEventInterface $event);

    // ~

    /**
     * @param GetCountryResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetCountryResponseEventInterface $event);

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterCountryResponseEventInterface $event);

}
