<?php

namespace Haru\CountryBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    Haru\CountryBundle\Event\FilterCountryResponseEventInterface,
    Haru\CountryBundle\Event\FormEventInterface,
    Haru\CountryBundle\Event\GetCountryResponseEventInterface;

/**
 * CountryListenerInterface interface.
 *
 * @package Haru\CountryBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CountryListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetCountryResponseEventInterface $event
     */
    public function onListInitialize(GetCountryResponseEventInterface $event);

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    public function onListCompleted(FilterCountryResponseEventInterface $event);

    // ~

    /**
     * @param GetCountryResponseEventInterface $event
     */
    public function onEditInitialize(GetCountryResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    public function onEditCompleted(FilterCountryResponseEventInterface $event);

    // ~

    /**
     * @param GetCountryResponseEventInterface $event
     */
    public function onDeleteInitialize(GetCountryResponseEventInterface $event);

    /**
     * @param FilterCountryResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterCountryResponseEventInterface $event);

}
