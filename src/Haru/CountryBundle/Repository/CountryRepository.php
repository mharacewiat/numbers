<?php

namespace Haru\CountryBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * CountryRepository class.
 *
 * @package Haru\CountryBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CountryRepository extends EntityRepository
{

}
