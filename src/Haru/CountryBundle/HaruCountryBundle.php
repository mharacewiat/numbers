<?php

namespace Haru\CountryBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HaruCountryBundle class.
 *
 * @package Haru\CountryBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruCountryBundle extends Bundle
{

}
