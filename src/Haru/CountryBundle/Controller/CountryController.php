<?php

namespace Haru\CountryBundle\Controller;

use Doctrine\DBAL\DBALException,
    Haru\CountryBundle\Entity\Country,
    Haru\CountryBundle\Event\FilterCountryResponseEvent,
    Haru\CountryBundle\Event\FilterCountryResponseEventInterface,
    Haru\CountryBundle\Event\FormEvent,
    Haru\CountryBundle\Event\FormEventInterface,
    Haru\CountryBundle\Event\GetCountryResponseEvent,
    Haru\CountryBundle\Form\CountryType,
    Haru\CountryBundle\Repository\CountryRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * CountryController class.
 *
 * @package Haru\CountryBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CountryController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to country list action. */
        return $this->redirectToRoute('haru_country_country_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'countries' => null,
        );

        // ~

        /* Prepare services. */

        $countryManager = $this->getCountryManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetCountryResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetCountryResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var CountryRepository $countryRepository */
        $countryRepository = $countryManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all countries. */
        $countries = $countryRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['countries'] = $countries;

        // ~

        $response = $this->render('HaruCountryBundle:Country:list.html.twig', $renderData);

        // ~

        $event = new FilterCountryResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterCountryResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'country' => null,
        );

        // ~

        /* Prepare services. */

        $countryManager = $this->getCountryManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var CountryRepository $countryRepository */
        $countryRepository = $countryManager->getRepository();

        /** @var Country $country */
        $country = $countryRepository->find($id);

        // ~

        if (true == is_null($country)) {
            /** @var Country $country */
            $country = $countryManager->create();
        }

        // ~

        $event = new GetCountryResponseEvent($country, $request);
        $eventDispatcher->dispatch(GetCountryResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new CountryType();
        $formOptions = array();

        $form = $this->createForm($formType, $country, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update country. */
                    $countryManager->update($country, true);

                    // ~

                    $event = new FilterCountryResponseEvent($country, $response);
                    $eventDispatcher->dispatch(FilterCountryResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('haru_country_country_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['country'] = $country;

        // ~

        return $this->render('HaruCountryBundle:Country:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $countryManager = $this->getCountryManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var CountryRepository $countryRepository */
        $countryRepository = $countryManager->getRepository();

        /** @var Country $country */
        $country = $countryRepository->find($id);

        // ~

        $event = new GetCountryResponseEvent($country, $request);
        $eventDispatcher->dispatch(GetCountryResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($country)) {
            try {
                /* Update country. */
                $countryManager->delete($country, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('haru_country_country_list', array());

        // ~

        $event = new FilterCountryResponseEvent($country, $response);
        $eventDispatcher->dispatch(FilterCountryResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
