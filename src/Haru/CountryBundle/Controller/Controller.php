<?php

namespace Haru\CountryBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    Haru\CountryBundle\Service\Manager as CountryManager;

/**
 * Controller abstract class.
 *
 * @package Haru\CountryBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return CountryManager
     */
    protected function getCountryManager()
    {
        return $this->getContainer()->get('haru_country.manager');
    }

}
