<?php

namespace Haru\CountryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection,
    Doctrine\Common\Collections\Collection,
    Haru\ORM\Mapping\Entity,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * Country class.
 *
 * @package Haru\CountryBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Country extends Entity implements CountryInterface
{

    // ~ Properties.

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $deleted_at;

    // ~

    /**
     * @var Collection
     */
    protected $provinces;

    // ~ Magic methods.

    /**
     * CountryInterface constructor.
     */
    public function __construct()
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->provinces = new ArrayCollection();

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CountryInterface
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CountryInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return CountryInterface
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    // ~

    /**
     * Add province
     *
     * @param ProvinceInterface $province
     *
     * @return CountryInterface
     */
    public function addProvince(ProvinceInterface $province)
    {
        $this->provinces[] = $province;

        return $this;
    }

    /**
     * Remove province
     *
     * @param ProvinceInterface $province
     */
    public function removeProvince(ProvinceInterface $province)
    {
        $this->provinces->removeElement($province);
    }

    /**
     * Get provinces
     *
     * @return Collection
     */
    public function getProvinces()
    {
        return $this->provinces;
    }

}
