<?php

namespace Haru\CountryBundle\Entity;

use Doctrine\Common\Collections\Collection,
    Haru\ORM\Mapping\EntityInterface,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * CountryInterface interface.
 *
 * @package Haru\CountryBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CountryInterface extends EntityInterface
{

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CountryInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CountryInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return CountryInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

    // ~

    /**
     * Add province
     *
     * @param ProvinceInterface $province
     *
     * @return CountryInterface
     */
    public function addProvince(ProvinceInterface $province);

    /**
     * Remove province
     *
     * @param ProvinceInterface $province
     */
    public function removeProvince(ProvinceInterface $province);

    /**
     * Get provinces
     *
     * @return Collection
     */
    public function getProvinces();

}
