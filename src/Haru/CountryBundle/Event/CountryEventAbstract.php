<?php

namespace Haru\CountryBundle\Event;

use Haru\Component\EventDispatcher\Event,
    Haru\CountryBundle\Entity\CountryInterface;

/**
 * CountryEventAbstract abstract class.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class CountryEventAbstract extends Event implements CountryEventInterface
{

    // ~ Properties.

    /**
     * @var CountryInterface|null
     */
    protected $country = null;

    // ~ Magic methods.

    /**
     * CountryEventAbstract constructor.
     *
     * @param CountryInterface|null $country
     */
    public function __construct(CountryInterface $country = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setCountry($country);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return CountryInterface|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param CountryInterface|null $country
     * @return $this
     */
    protected function setCountry(CountryInterface $country = null)
    {
        $this->country = $country;
        return $this;
    }

}
