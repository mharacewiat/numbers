<?php

namespace Haru\CountryBundle\Event;

/**
 * GetCountryResponseEvent class.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetCountryResponseEvent extends GetCountryResponseEventAbstract
{

}
