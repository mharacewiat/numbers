<?php

namespace Haru\CountryBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetCountryResponseEventInterface interface.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetCountryResponseEventInterface extends CountryEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'haru_country.country.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'haru_country.country.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'haru_country.country.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
