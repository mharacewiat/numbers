<?php

namespace Haru\CountryBundle\Event;

/**
 * FilterCountryResponseEventAbstract abstract class.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterCountryResponseEventAbstract extends CountryEventAbstract implements FilterCountryResponseEventInterface
{

}
