<?php

namespace Haru\CountryBundle\Event;

/**
 * FilterCountryResponseEventInterface interface.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterCountryResponseEventInterface extends CountryEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'haru_country.country.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'haru_country.country.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'haru_country.country.delete.completed';

}
