<?php

namespace Haru\CountryBundle\Event;

use Haru\CountryBundle\Entity\CountryInterface,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GetCountryResponseEventAbstract abstract class.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GetCountryResponseEventAbstract extends CountryEvent implements GetCountryResponseEventInterface
{

    // ~ Properties.

    /**
     * @var Response|null
     */
    protected $response = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    // ~ Magic methods.

    /**
     * GetCountryResponseEventAbstract constructor.
     *
     * @param CountryInterface|null $country
     * @param Request|null $request
     */
    public function __construct(CountryInterface $country = null, Request $request = null)
    {
        /* Construct parent. */
        parent::__construct($country);

        // ~

        $this->setRequest($request);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     * @return $this
     */
    protected function setResponse(Response $response = null)
    {
        $this->response = $response;
        return $this;
    }

    // ~

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     * @return $this
     */
    protected function setRequest(Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

}
