<?php

namespace Haru\CountryBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    Haru\CountryBundle\Entity\CountryInterface;

/**
 * CountryEventInterface interface.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CountryEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return CountryInterface|null
     */
    public function getCountry();

}
