<?php

namespace Haru\CountryBundle\Event;

/**
 * FilterCountryResponseEvent class.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterCountryResponseEvent extends FilterCountryResponseEventAbstract
{

}
