<?php

namespace Haru\CountryBundle\Event;

use Haru\Component\EventDispatcher\FormEvent as BaseFormEvent;

/**
 * FormEventAbstract abstract class.
 *
 * @package Haru\CountryBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FormEventAbstract extends BaseFormEvent implements FormEventInterface
{

}
