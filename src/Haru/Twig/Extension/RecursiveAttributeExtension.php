<?php

namespace Haru\Twig\Extension;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * RecursiveAttributeExtension class.
 *
 * @package Haru\Twig\Extension
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RecursiveAttributeExtension extends \Twig_Extension
{

    // ~ Properties.

    /**
     * @var PropertyAccessorInterface
     */
    protected $propertyAccessor;

    // ~ Magic methods.

    /**
     * RecursiveAttributeExtension constructor.
     *
     * @param PropertyAccessorInterface $propertyAccessor
     */
    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        ///* Parent construct. */
        //parent::__construct();

        // ~

        $this->setPropertyAccessor($propertyAccessor);

        // ~

        return;
    }

    // ~ Functions.

    /**
     * @param $object
     * @param array $properties
     * @return mixed|null
     */
    public function recursiveAttribute($object, array $properties)
    {
        $propertyAccessor = $this->getPropertyAccessor();

        // ~

        if (
            (count($properties) > 0) &&
            (true == $propertyAccessor->isReadable($object, $property = array_shift($properties)))
        ) {
            $object = $propertyAccessor->getValue($object, $property);
        }

        // ~

        if (
            (false == is_null($object)) &&
            (count($properties) > 0)
        ) {
            return call_user_func_array(array($this, 'recursiveAttribute'), array($object, $properties));
        }

        // ~

        return $object;
    }

    // ~  Getters and setters.

    /**
     * @return array
     */
    public function getFunctions()
    {
        $functions = array(
            new \Twig_SimpleFunction('recursive_attribute', array($this, 'recursiveAttribute')),
        );

        // ~

        return $functions;
    }

    // ~

    /**
     * @return string
     */
    public function getName()
    {
        return 'recursive_attribute_extension';
    }

    // ~

    /**
     * @return PropertyAccessorInterface
     */
    public function getPropertyAccessor()
    {
        return $this->propertyAccessor;
    }

    /**
     * @param PropertyAccessorInterface $propertyAccessor
     * @return RecursiveAttributeExtension
     */
    public function setPropertyAccessor(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
        return $this;
    }

}
