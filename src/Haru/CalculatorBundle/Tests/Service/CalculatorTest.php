<?php

namespace Haru\CalculatorBundle\Tests\Service;

use Haru\Bundle\FrameworkBundle\Test\KernelTestCase,
    Haru\CalculatorBundle\Service\Calculator as CalculatorService;

/**
 * CalculatorTest class.
 *
 * @package Haru\CalculatorBundle\Tests\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CalculatorTest extends KernelTestCase
{

    // ~ Tests.

    /**
     * @dataProvider getCalculateOffsetTestData
     *
     * @param $page
     * @param $limit
     * @param $offset
     */
    public function testCalculateOffset($page, $limit, $offset)
    {
        /* Get calculator. */
        $calculatorMock = $this->getCalculatorService();

        // ~

        /* Asserts. */

        $this->assertEquals(
            $offset,
            $calculatorMock->calculateOffset($page, $limit)
        );

        // ~

        return;
    }

    // ~ Data providers.

    /**
     * @return array
     */
    public function getCalculateOffsetTestData()
    {
        $data = array(
            array(-2, -10, 0),
            array(-1, -10, 0),
            array(0, -10, 0),
            array(1, -10, 0),
            array(2, -10, 0),
            // ~
            array(-2, 0, 0),
            array(-1, 0, 0),
            array(0, 0, 0),
            array(1, 0, 0),
            array(2, 0, 0),
            // ~
            array(-2, 10, 0),
            array(-1, 10, 0),
            array(0, 10, 0),
            array(1, 10, 0),
            array(2, 10, 10),
            // ~
            array(3, 10, 20),
            array(4, 10, 30),
            array(5, 10, 40),
            array(6, 10, 50),
            array(7, 10, 60),
            array(8, 10, 70),
            array(9, 10, 80),
            // ~
            array(1, 20, 0),
            array(2, 20, 20),
            array(3, 20, 40),
            array(4, 20, 60),
            array(5, 20, 80),
            array(6, 20, 100),
            array(7, 20, 120),
            array(8, 20, 140),
            array(9, 20, 160),
        );

        // ~

        return $data;
    }


    // ~ Getters and setters.

    /**
     * @return CalculatorService
     */
    protected function getCalculatorService()
    {
        return $this->getKernel()->getContainer()->get('haru_calculator.calculator');
    }

}
