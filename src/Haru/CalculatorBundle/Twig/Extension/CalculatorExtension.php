<?php

namespace Haru\CalculatorBundle\Twig\Extension;

use Haru\CalculatorBundle\Service\Calculator as CalculatorService;

/**
 * CalculatorExtension class.
 *
 * @package Haru\CalculatorBundle\Twig\Extension
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CalculatorExtension extends \Twig_Extension
{

    // ~ Properties.

    /**
     * @var CalculatorService
     */
    protected $calculator;

    // ~ Magic methods.

    /**
     * CalculatorExtension constructor.
     *
     * @param CalculatorService $calculator
     */
    public function __construct(CalculatorService $calculator)
    {
        $this->setCalculator($calculator);

        // ~

        return;
    }

    // ~ Functions.

    /**
     * @param $page
     * @param $limit
     * @return int
     */
    public function calculateOffset($page, $limit)
    {
        return $this->getCalculator()->calculateOffset($page, $limit);
    }

    /**
     * @param $limit
     * @param $count
     * @return float|int
     */
    public function calculatePages($limit, $count)
    {
        return $this->getCalculator()->calculatePages($limit, $count);
    }

    // ~  Getters and setters.

    /**
     * @return array
     */
    public function getFunctions()
    {
        $functions = array(
            new \Twig_SimpleFunction('calculate_offset', array($this, 'calculateOffset')),
            new \Twig_SimpleFunction('calculate_pages', array($this, 'calculatePages')),
        );

        // ~

        return $functions;
    }

    // ~

    /**
     * @return string
     */
    public function getName()
    {
        return 'calculator_extension';
    }

    // ~

    /**
     * @return CalculatorService
     */
    public function getCalculator()
    {
        return $this->calculator;
    }

    /**
     * @param CalculatorService $calculator
     * @return CalculatorExtension
     */
    public function setCalculator(CalculatorService $calculator)
    {
        $this->calculator = $calculator;
        return $this;
    }

}
