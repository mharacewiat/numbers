<?php

namespace Haru\CalculatorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HaruCalculatorBundle class.
 *
 * @package Haru\CalculatorBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruCalculatorBundle extends Bundle
{

}
