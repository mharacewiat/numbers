<?php

namespace Haru\CalculatorBundle\Service;

/**
 * CalculatorAbstract abstract class.
 *
 * @package Haru\CalculatorBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class CalculatorAbstract implements CalculatorInterface
{

    // ~ Properties.

    /**
     * @param $page
     * @param $limit
     * @return int
     */
    public function calculateOffset($page, $limit)
    {
        if (
            ($page <= 1) ||
            ($limit <= 0)
        ) {
            return 0;
        }

        // ~

        return (int)(--$page * $limit);
    }

    /**
     * @param $limit
     * @param $count
     * @return float|int
     */
    public function calculatePages($limit, $count)
    {
        if (
            (
                ($limit < 0) ||
                ($count < 0)
            ) ||
            ($limit > $count)
        ) {
            return 1;
        }

        // ~

        return ceil($count / $limit);
    }

}
