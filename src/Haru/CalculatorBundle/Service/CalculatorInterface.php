<?php

namespace Haru\CalculatorBundle\Service;

/**
 * CalculatorInterface interface.
 *
 * @package Haru\CalculatorBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CalculatorInterface
{

    // ~ Main methods.

    /**
     * @param $page
     * @param $limit
     * @return int
     */
    public function calculateOffset($page, $limit);

}
