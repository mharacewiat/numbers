<?php

namespace Haru\Bundle\FrameworkBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as BaseKernelTestCase,
    Symfony\Component\HttpKernel\KernelInterface;

/**
 * KernelTestCase class.
 *
 * @package Haru\Bundle\FrameworkBundle\Test
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class KernelTestCase extends BaseKernelTestCase
{

    /**
     * @return KernelInterface
     */
    protected function getKernel()
    {
        return self::$kernel;
    }

}
