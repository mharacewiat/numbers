<?php

namespace Haru\Bundle\FrameworkBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

/**
 * WebTestCase abstract class.
 *
 * @package Haru\Bundle\FrameworkBundle\Test
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class WebTestCase extends BaseWebTestCase
{

}
