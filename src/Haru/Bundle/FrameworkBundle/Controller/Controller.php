<?php

namespace Haru\Bundle\FrameworkBundle\Controller;

use Haru\CalculatorBundle\Service\Calculator as CalculatorService,
    Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    Symfony\Component\DependencyInjection\ContainerInterface,
    Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Controller abstract class.
 *
 * @package Haru\Bundle\FrameworkBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return CalculatorService
     */
    protected function getCalculatorService()
    {
        return $this->getContainer()->get('haru_calculator.calculator');
    }

    /**
     * @return EventDispatcherInterface
     */
    protected function getEventDispatcher()
    {
        return $this->getContainer()->get('event_dispatcher');
    }

    // ~

    /**
     * Container getter.
     *
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }

}
