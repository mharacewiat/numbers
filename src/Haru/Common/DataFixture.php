<?php

namespace Haru\Common;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\DataFixtures\OrderedFixtureInterface,
    Haru\Component\DependencyInjection\ContainerAwareTrait,
    Symfony\Component\DependencyInjection\ContainerAwareInterface,
    Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException,
    Symfony\Component\Yaml\Parser as YamlParser;

/**
 * DataFixture abstract class.
 *
 * @package Haru\Common
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class DataFixture extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{

    // ~ Traits.

    /**
     * Container aware trait.
     */
    use ContainerAwareTrait;

    // ~ Helpers.

    /**
     * @param $file
     * @return mixed
     */
    protected function loadData($file)
    {
        /* Get file path. */
        $path = $this->getFilePath($file);

        // ~

        $parser = new YamlParser();
        $data = $parser->parse(file_get_contents($path));

        // ~

        return $data;
    }

    // ~

    /**
     * @param array $fields
     * @param array $data
     * @return array
     */
    protected function fillData(array $fields, array $data)
    {
        //if (true == function_exists('array_except')) {
        //    $data = array_except($data, $fields);
        //}

        // ~

        /* Fill up keys with defaults. */
        $defaults = array_fill_keys($fields, null);

        // ~

        /* Replace defaults with data. */
        $filledData = array_replace($defaults, $data);

        /* Get rid of keys. */
        $filledData = array_values($filledData);

        // ~

        return $filledData;
    }

    // ~ Getters and setters.

    /**
     * @param $file
     * @return string
     */
    protected function getFilePath($file)
    {
        $reflectionClass = new \ReflectionClass($this);

        // ~

        $classPath = $reflectionClass->getFileName();
        $classDirectory = dirname($classPath);

        // ~

        $filePathParts = array(
            $classDirectory,
            '..',
            '..',
            'Resources',
            'data',
            $file . '.yml',
        );

        $rawFilePath = implode(DIRECTORY_SEPARATOR, $filePathParts);
        $filePath = realpath($rawFilePath);

        // ~

        if (false === $filePath) {
            throw new FileNotFoundException($rawFilePath);
        }

        // ~

        return $filePath;
    }

    // ~

    /**
     * Order getter.
     *
     * @return int
     */
    public function getOrder()
    {
        return 0;
    }

}
