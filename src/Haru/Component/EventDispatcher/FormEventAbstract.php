<?php

namespace Haru\Component\EventDispatcher;

use Symfony\Component\Form\FormInterface;

/**
 * FormEventAbstract abstract class.
 *
 * @package Haru\Component\EventDispatcher
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FormEventAbstract extends Event implements FormEventInterface
{

    // ~ Properties.

    /**
     * @var FormInterface
     */
    protected $form;

    // ~ Magic methods.

    /**
     * FormEventAbstract constructor.
     *
     * @param FormInterface $form
     */
    public function __construct(FormInterface $form)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setForm($form);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param FormInterface $form
     * @return FormEventAbstract
     */
    protected function setForm(FormInterface $form)
    {
        $this->form = $form;
        return $this;
    }

}
