<?php

namespace Haru\Component\EventDispatcher;

/**
 * EventListener abstract class.
 *
 * @package Haru\Component\EventDispatcher
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class EventListener extends EventListenerAbstract
{

}
