<?php

namespace Haru\Component\EventDispatcher;

use Haru\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface,
    Symfony\Component\HttpFoundation\Session\SessionBagInterface;

/**
 * EventListenerAbstract abstract class.
 *
 * @package Haru\Component\EventDispatcher
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class EventListenerAbstract extends ContainerAware implements EventListenerInterface
{

    // ~ Getters and setters.

    /**
     * @return FlashBagInterface|SessionBagInterface
     */
    protected function getFlashMessenger()
    {
        return $this->getContainer()->get('session')->getFlashBag();
    }

}
