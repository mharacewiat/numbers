<?php

namespace Haru\Component\EventDispatcher;

use Symfony\Component\Form\FormInterface;

/**
 * FormEventInterface interface.
 *
 * @package Haru\Component\EventDispatcher
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FormEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return FormInterface
     */
    public function getForm();

}
