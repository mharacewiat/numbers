<?php

namespace Haru\Component\EventDispatcher;

use Symfony\Component\EventDispatcher\Event as BaseEvent;

/**
 * Event class.
 *
 * @package Haru\Component\EventDispatcher
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Event extends BaseEvent implements EventInterface
{

}
