<?php

namespace Haru\Component\Form;

use Symfony\Component\Form\AbstractType as BaseAbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormEvent,
    Symfony\Component\Form\FormEvents,
    Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * AbstractType abstract class.
 *
 * @package Haru\Component\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class AbstractType extends BaseAbstractType
{

    // ~ Main methods.

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* Build parent form. */
        parent::buildForm($builder, $options);

        // ~

        /* Register listeners. s*/
        $this->registerListeners($builder);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param FormBuilderInterface $builder
     */
    protected function registerListeners(FormBuilderInterface $builder)
    {
        /* Register submit event listener. */
        $builder->addEventListener(FormEvents::POST_SET_DATA, array($this, 'submitListener'), 0);

        // ~

        return;
    }

    // ~

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /* Configure parent options. */
        parent::configureOptions($resolver);

        // ~

        $resolver->setDefaults(array(
            'submit' => true,
        ));

        // ~

        $resolver->addAllowedTypes('submit', array(
            'bool',
            'int',
        ));

        // ~


        return;
    }

    // ~ Events.

    /**
     * @param FormEvent $event
     */
    public function submitListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $submit */
        $submit = $form->getConfig()->getOption('submit');

        // ~

        if (true == $submit) {
            /* Submit. */
            $form->add('submit', 'submit', array(
                'label' => 'form.submit',
            ));
        }

        // ~

        return;
    }

}
