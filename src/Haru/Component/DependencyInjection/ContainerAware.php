<?php

namespace Haru\Component\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ContainerAware abstract class.
 *
 * @package Haru\Component\DependencyInjection
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ContainerAware
{

    // ~ Traits.

    /**
     * Container aware trait.
     */
    use ContainerAwareTrait;

    // ~ Magic methods.

    /**
     * ContainerAware constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setContainer($container);

        // ~

        return;
    }

}
