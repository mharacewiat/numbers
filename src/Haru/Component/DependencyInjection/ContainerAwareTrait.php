<?php

namespace Haru\Component\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerAwareTrait as BaseContainerAwareTrait,
    Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ContainerAwareTrait class.
 *
 * @package Haru\Component\DependencyInjection
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
trait ContainerAwareTrait
{

    // ~ Traits

    /**
     * Base container aware trait.
     */
    use BaseContainerAwareTrait;

    // ~  Getters and setters.

    /**
     * Container getter.
     *
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }

}
