<?php

namespace Haru\RoleBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\Common\DataFixture,
    Haru\RoleBundle\Entity\Role,
    Haru\RoleBundle\Service\Manager as RoleManager;

/**
 * Roles class.
 *
 * @package Haru\RoleBundle\DataFixtures\ORM
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Roles extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load roles data. */
        $rolesData = $this->loadRolesData();

        // ~

        /* Load roles. */
        $this->loadRoles($rolesData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $rolesData
     * @param ObjectManager $manager
     */
    protected function loadRoles(array $rolesData, ObjectManager $manager)
    {
        /* Iterate through roles data. */
        foreach ($rolesData as $key => $value) {
            /* Load role. */
            $this->loadRole($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $roleData
     * @param ObjectManager $manager
     */
    protected function loadRole($key, array $roleData, ObjectManager $manager)
    {
        /* Build role. */
        $roleEntity = $this->buildRole($roleData, $manager);

        // ~

        /* Store role. */
        $this->storeRole($key, $roleEntity);

        // ~

        return;
    }

    // ~

    /**
     * @param array $roleData
     * @param ObjectManager $manager
     * @return Role
     */
    protected function buildRole(array $roleData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $roleManager = $this->getRoleManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $name
         * @var string $role
         */
        list(
            $name,
            $role
            ) = $this->prepareData($roleData);

        // ~

        $roleEntity = $roleManager->create();

        // ~

        $roleEntity->setName($name);
        $roleEntity->setRole($role);

        // ~

        $roleManager->update($roleEntity, false);

        // ~

        return $roleEntity;
    }

    // ~

    /**
     * @param $key
     * @param Role $role
     * @return $this
     */
    protected function storeRole($key, Role $role)
    {
        $this->setReference(vsprintf('role-%1$s', array($key)), $role);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadRolesData()
    {
        return $this->loadData('roles');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('name', 'role'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return RoleManager
     */
    protected function getRoleManager()
    {
        return $this->getContainer()->get('haru_role.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 101;
    }

}
