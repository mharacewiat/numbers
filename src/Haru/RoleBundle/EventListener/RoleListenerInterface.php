<?php

namespace Haru\RoleBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    Haru\RoleBundle\Event\FilterRoleResponseEventInterface,
    Haru\RoleBundle\Event\FormEventInterface,
    Haru\RoleBundle\Event\GetRoleResponseEventInterface;

/**
 * RoleListenerInterface interface.
 *
 * @package Haru\RoleBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface RoleListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetRoleResponseEventInterface $event
     */
    public function onListInitialize(GetRoleResponseEventInterface $event);

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    public function onListCompleted(FilterRoleResponseEventInterface $event);

    // ~

    /**
     * @param GetRoleResponseEventInterface $event
     */
    public function onEditInitialize(GetRoleResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    public function onEditCompleted(FilterRoleResponseEventInterface $event);

    // ~

    /**
     * @param GetRoleResponseEventInterface $event
     */
    public function onDeleteInitialize(GetRoleResponseEventInterface $event);

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterRoleResponseEventInterface $event);

}
