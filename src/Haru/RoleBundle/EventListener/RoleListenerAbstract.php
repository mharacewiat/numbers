<?php

namespace Haru\RoleBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    Haru\RoleBundle\Event\FilterRoleResponseEventInterface,
    Haru\RoleBundle\Event\FormEventInterface,
    Haru\RoleBundle\Event\GetRoleResponseEventInterface;

/**
 * RoleListenerAbstract abstract class.
 *
 * @package Haru\RoleBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class RoleListenerAbstract extends EventListener implements RoleListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetRoleResponseEventInterface $event
     */
    abstract public function onListInitialize(GetRoleResponseEventInterface $event);

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterRoleResponseEventInterface $event);

    // ~

    /**
     * @param GetRoleResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetRoleResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterRoleResponseEventInterface $event);

    // ~

    /**
     * @param GetRoleResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetRoleResponseEventInterface $event);

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterRoleResponseEventInterface $event);

}
