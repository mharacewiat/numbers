<?php

namespace Haru\RoleBundle\EventListener;

use Haru\RoleBundle\Event\FilterRoleResponseEventInterface,
    Haru\RoleBundle\Event\FormEventInterface,
    Haru\RoleBundle\Event\GetRoleResponseEventInterface;

/**
 * RoleListener class.
 *
 * @package Haru\RoleBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RoleListener extends RoleListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetRoleResponseEventInterface $event
     */
    public function onListInitialize(GetRoleResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    public function onListCompleted(FilterRoleResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetRoleResponseEventInterface $event
     */
    public function onEditInitialize(GetRoleResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    public function onEditCompleted(FilterRoleResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'role.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetRoleResponseEventInterface $event
     */
    public function onDeleteInitialize(GetRoleResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterRoleResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterRoleResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'role.delete');

        // ~

        return;
    }

}
