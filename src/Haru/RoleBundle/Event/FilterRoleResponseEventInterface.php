<?php

namespace Haru\RoleBundle\Event;

/**
 * FilterRoleResponseEventInterface interface.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterRoleResponseEventInterface extends RoleEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'haru_role.role.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'haru_role.role.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'haru_role.role.delete.completed';

}
