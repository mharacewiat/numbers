<?php

namespace Haru\RoleBundle\Event;

/**
 * FilterRoleResponseEventAbstract abstract class.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterRoleResponseEventAbstract extends RoleEventAbstract implements FilterRoleResponseEventInterface
{

}
