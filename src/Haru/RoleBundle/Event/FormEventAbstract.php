<?php

namespace Haru\RoleBundle\Event;

use Haru\Component\EventDispatcher\FormEvent as BaseFormEvent;

/**
 * FormEventAbstract abstract class.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FormEventAbstract extends BaseFormEvent implements FormEventInterface
{

}
