<?php

namespace Haru\RoleBundle\Event;

use Haru\Component\EventDispatcher\Event,
    Haru\RoleBundle\Entity\RoleInterface;

/**
 * RoleEventAbstract abstract class.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class RoleEventAbstract extends Event implements RoleEventInterface
{

    // ~ Properties.

    /**
     * @var RoleInterface|null
     */
    protected $role = null;

    // ~ Magic methods.

    /**
     * RoleEventAbstract constructor.
     *
     * @param RoleInterface|null $role
     */
    public function __construct(RoleInterface $role = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setRole($role);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return RoleInterface|null
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param RoleInterface|null $role
     * @return $this
     */
    protected function setRole(RoleInterface $role = null)
    {
        $this->role = $role;
        return $this;
    }

}
