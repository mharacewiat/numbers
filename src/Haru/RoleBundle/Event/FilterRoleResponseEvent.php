<?php

namespace Haru\RoleBundle\Event;

/**
 * FilterRoleResponseEvent class.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterRoleResponseEvent extends FilterRoleResponseEventAbstract
{

}
