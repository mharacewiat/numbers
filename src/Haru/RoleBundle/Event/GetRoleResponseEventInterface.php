<?php

namespace Haru\RoleBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetRoleResponseEventInterface interface.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetRoleResponseEventInterface extends RoleEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'haru_role.role.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'haru_role.role.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'haru_role.role.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
