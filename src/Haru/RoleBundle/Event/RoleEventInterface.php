<?php

namespace Haru\RoleBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    Haru\RoleBundle\Entity\RoleInterface;

/**
 * RoleEventInterface interface.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface RoleEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return RoleInterface|null
     */
    public function getRole();

}
