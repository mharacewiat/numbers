<?php

namespace Haru\RoleBundle\Event;

/**
 * GetRoleResponseEvent class.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetRoleResponseEvent extends GetRoleResponseEventAbstract
{

}
