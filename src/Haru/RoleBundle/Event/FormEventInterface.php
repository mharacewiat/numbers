<?php

namespace Haru\RoleBundle\Event;

use Haru\Component\EventDispatcher\FormEventInterface as BaseFormEventInterface;

/**
 * FormEventInterface interface.
 *
 * @package Haru\RoleBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FormEventInterface extends BaseFormEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const EDIT_SUCCESS = 'haru_role.role.edit.success';

}
