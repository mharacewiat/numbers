<?php

namespace Haru\RoleBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    Haru\RoleBundle\Service\Manager as RoleManager;

/**
 * Controller abstract class.
 *
 * @package Haru\RoleBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return RoleManager
     */
    protected function getRoleManager()
    {
        return $this->getContainer()->get('haru_role.manager');
    }

}
