<?php

namespace Haru\RoleBundle\Controller;

use Doctrine\DBAL\DBALException,
    Haru\RoleBundle\Entity\Role,
    Haru\RoleBundle\Event\FilterRoleResponseEvent,
    Haru\RoleBundle\Event\FilterRoleResponseEventInterface,
    Haru\RoleBundle\Event\FormEvent,
    Haru\RoleBundle\Event\FormEventInterface,
    Haru\RoleBundle\Event\GetRoleResponseEvent,
    Haru\RoleBundle\Form\RoleType,
    Haru\RoleBundle\Repository\RoleRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * RoleController class.
 *
 * @package Haru\RoleBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RoleController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to role list action. */
        return $this->redirectToRoute('haru_role_role_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'roles' => null,
        );

        // ~

        /* Prepare services. */

        $roleManager = $this->getRoleManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetRoleResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetRoleResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var RoleRepository $roleRepository */
        $roleRepository = $roleManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all roles. */
        $roles = $roleRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['roles'] = $roles;

        // ~

        $response = $this->render('HaruRoleBundle:Role:list.html.twig', $renderData);

        // ~

        $event = new FilterRoleResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterRoleResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'role' => null,
        );

        // ~

        /* Prepare services. */

        $roleManager = $this->getRoleManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var RoleRepository $roleRepository */
        $roleRepository = $roleManager->getRepository();

        /** @var Role $role */
        $role = $roleRepository->find($id);

        // ~

        if (true == is_null($role)) {
            /** @var Role $role */
            $role = $roleManager->create();
        }

        // ~

        $event = new GetRoleResponseEvent($role, $request);
        $eventDispatcher->dispatch(GetRoleResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new RoleType();
        $formOptions = array();

        $form = $this->createForm($formType, $role, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update role. */
                    $roleManager->update($role, true);

                    // ~

                    $event = new FilterRoleResponseEvent($role, $response);
                    $eventDispatcher->dispatch(FilterRoleResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('haru_role_role_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['role'] = $role;

        // ~

        return $this->render('HaruRoleBundle:Role:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $roleManager = $this->getRoleManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var RoleRepository $roleRepository */
        $roleRepository = $roleManager->getRepository();

        /** @var Role $role */
        $role = $roleRepository->find($id);

        // ~

        $event = new GetRoleResponseEvent($role, $request);
        $eventDispatcher->dispatch(GetRoleResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($role)) {
            try {
                /* Update role. */
                $roleManager->delete($role, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('haru_role_role_list', array());

        // ~

        $event = new FilterRoleResponseEvent($role, $response);
        $eventDispatcher->dispatch(FilterRoleResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
