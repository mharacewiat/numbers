<?php

namespace Haru\RoleBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * IndexController class.
 *
 * @package Haru\RoleBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to role index action. */
        return $this->redirectToRoute('haru_role_role_index', array());
    }

}
