<?php

namespace Haru\RoleBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HaruRoleBundle class.
 *
 * @package Haru\RoleBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruRoleBundle extends Bundle
{

}
