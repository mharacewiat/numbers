<?php

namespace Haru\RoleBundle\Service;

use Doctrine\Common\Persistence\ObjectManager,
    Doctrine\Common\Persistence\ObjectRepository,
    Haru\RoleBundle\Entity\RoleInterface;

/**
 * ManagerAbstract abstract class.
 *
 * @package Haru\RoleBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ManagerAbstract implements ManagerInterface
{

    // ~ Properties.

    /**
     * @var string
     */
    protected $class;

    // ~

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    // ~ Magic methods.

    /**
     * ManagerAbstract constructor.
     *
     * @param $class
     * @param ObjectManager $objectManager
     */
    public function __construct($class, ObjectManager $objectManager)
    {
        $this
            ->setClass($class)
            ->setObjectManager($objectManager);

        // ~

        return;
    }

    // ~ Main methods.

    /**
     * @return RoleInterface
     */
    public function create()
    {
        $reflectionClass = new \ReflectionClass($this->getClass());
        $instance = $reflectionClass->newInstanceArgs(func_get_args());

        // ~

        return $instance;
    }

    /**
     * @param RoleInterface $role
     * @param bool $flush
     * @return $this
     */
    public function update(RoleInterface $role, $flush = true)
    {
        $objectManager = $this->getObjectManager();

        // ~

        $objectManager->persist($role);

        // ~

        if (true == $flush) {
            $objectManager->flush();
        }

        // ~

        return $this;
    }

    /**
     * @param RoleInterface $role
     * @param bool $flush
     * @return $this
     */
    public function delete(RoleInterface $role, $flush = true)
    {
        $objectManager = $this->getObjectManager();

        // ~

        $objectManager->remove($role);

        // ~

        if (true == $flush) {
            $objectManager->flush();
        }

        // ~

        return $this;
    }

    // ~ Getters and setters.

    /**
     * Repository getter.
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        if (true == is_null($objectManager = $this->getObjectManager())) {
            throw new \InvalidArgumentException('Object manager does not exist');
        }

        // ~

        $class = $this->getClass();
        $repository = $objectManager->getRepository($class);

        // ~

        return $repository;
    }

    // ~

    /**
     * Class getter.
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Class setter.
     *
     * @param string $class
     * @return ManagerAbstract
     */
    protected function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    // ~

    /**
     * ObjectManager getter.
     *
     * @return ObjectManager
     */
    protected function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * ObjectManager setter.
     *
     * @param ObjectManager $objectManager
     * @return ManagerAbstract
     */
    protected function setObjectManager($objectManager)
    {
        $this->objectManager = $objectManager;
        return $this;
    }

}
