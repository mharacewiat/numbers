<?php

namespace Haru\RoleBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    Haru\RoleBundle\Entity\RoleInterface;

/**
 * ManagerInterface interface.
 *
 * @package Haru\RoleBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return RoleInterface
     */
    public function create();

    /**
     * @param RoleInterface $role
     * @param bool $flush
     * @return $this
     */
    public function update(RoleInterface $role, $flush = true);

    /**
     * @param RoleInterface $role
     * @param bool $flush
     * @return $this
     */
    public function delete(RoleInterface $role, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~

    /**
     * @return string
     */
    public function getClass();

}
