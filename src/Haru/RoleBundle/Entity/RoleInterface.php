<?php

namespace Haru\RoleBundle\Entity;

use Doctrine\Common\Collections\Collection,
    Haru\ORM\Mapping\EntityInterface,
    Haru\UserBundle\Entity\GroupInterface;

/**
 * RoleInterface interface.
 *
 * @package Haru\RoleBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface RoleInterface extends EntityInterface
{

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RoleInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set role
     *
     * @param string $role
     *
     * @return RoleInterface
     */
    public function setRole($role);

    /**
     * Get role
     *
     * @return string
     */
    public function getRole();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return RoleInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return RoleInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

    // ~

    /**
     * Add group
     *
     * @param GroupInterface $group
     *
     * @return RoleInterface
     */
    public function addGroup(GroupInterface $group);

    /**
     * Remove group
     *
     * @param GroupInterface $group
     */
    public function removeGroup(GroupInterface $group);

    /**
     * Get groups
     *
     * @return Collection
     */
    public function getGroups();

}
