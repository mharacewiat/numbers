<?php

namespace Haru\RoleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection,
    Doctrine\Common\Collections\Collection,
    Haru\ORM\Mapping\Entity,
    Haru\UserBundle\Entity\GroupInterface;

/**
 * Role class.
 *
 * @package Haru\RoleBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Role extends Entity implements RoleInterface
{

    // ~ Properties.

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $role;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $deleted_at;

    // ~

    /**
     * @var Collection
     */
    private $groups;

    // ~ Magic methods.

    /**
     * RoleInterface constructor.
     */
    public function __construct()
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->groups = new ArrayCollection();

        // ~

        return;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getRole();
    }

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RoleInterface
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return RoleInterface
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return RoleInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return RoleInterface
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    // ~

    /**
     * Add group
     *
     * @param GroupInterface $group
     *
     * @return RoleInterface
     */
    public function addGroup(GroupInterface $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param GroupInterface $group
     */
    public function removeGroup(GroupInterface $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

}
