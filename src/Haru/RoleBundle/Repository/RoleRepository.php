<?php

namespace Haru\RoleBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * RoleRepository class.
 *
 * @package Haru\RoleBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RoleRepository extends EntityRepository
{

}
