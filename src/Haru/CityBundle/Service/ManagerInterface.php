<?php

namespace Haru\CityBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    Haru\CityBundle\Entity\CityInterface;

/**
 * ManagerInterface interface.
 *
 * @package Haru\CityBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return CityInterface
     */
    public function create();

    /**
     * @param CityInterface $city
     * @param bool $flush
     * @return $this
     */
    public function update(CityInterface $city, $flush = true);

    /**
     * @param CityInterface $city
     * @param bool $flush
     * @return $this
     */
    public function delete(CityInterface $city, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~

    /**
     * @return string
     */
    public function getClass();

}
