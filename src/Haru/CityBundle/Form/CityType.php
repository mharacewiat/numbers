<?php

namespace Haru\CityBundle\Form;

use Haru\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormEvent,
    Symfony\Component\Form\FormEvents,
    Symfony\Component\OptionsResolver\OptionsResolver,
    Symfony\Component\Validator\Constraints\Date;

/**
 * CityType class.
 *
 * @package Haru\CityBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CityType extends AbstractType
{

    // ~ Main methods.

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* Build parent form. */
        parent::buildForm($builder, $options);

        // ~

        /* Name. */
        $builder->add('name', 'text', array(
            'label' => 'haru.city.city.name',
        ));

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param FormBuilderInterface $builder
     */
    protected function registerListeners(FormBuilderInterface $builder)
    {
        /* Register parent listeners. */
        parent::registerListeners($builder);

        // ~

        /* Register created at event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'createdAtListener'), 0);

        /* Register deleted at event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'deletedAtListener'), 0);

        // ~

        /* Register province event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'provinceListener'), 0);

        // ~

        return;
    }

    // ~

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /* Configure parent options. */
        parent::configureOptions($resolver);

        // ~

        $resolver->setDefaults(array(
            'data_class' => 'Haru\\CityBundle\\Entity\\City',
            // ~
            'created_at' => false,
            'deleted_at' => false,
            // ~
            'province' => true,
            // ~
            'submit' => true,
        ));

        // ~

        $resolver->addAllowedTypes('created_at', array(
            'bool',
            'int',
        ));

        $resolver->addAllowedTypes('deleted_at', array(
            'bool',
            'int',
        ));

        $resolver->addAllowedTypes('province', array(
            'bool',
            'int',
        ));

        // ~

        return;
    }

    // ~ Events.

    /**
     * @param FormEvent $event
     */
    public function createdAtListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $createdAt */
        $createdAt = $form->getConfig()->getOption('created_at');

        // ~

        if (true == $createdAt) {
            /* Created at. */
            $form->add('created_at', 'datetime', array(
                'label' => 'haru.city.city.created_at',
            ));
        }

        // ~

        return;
    }

    /**
     * @param FormEvent $event
     */
    public function deletedAtListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $deletedAt */
        $deletedAt = $form->getConfig()->getOption('deleted_at');

        // ~

        if (true == $deletedAt) {
            /* Deleted at. */
            $form->add('deleted_at', 'datetime', array(
                'constraints' => array(
                    new Date(),
                ),
                'label' => 'haru.city.city.deleted_at',
                'required' => false,
            ));
        }

        // ~

        return;
    }

    // ~

    /**
     * @param FormEvent $event
     */
    public function provinceListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $province */
        $province = $form->getConfig()->getOption('province');

        // ~

        if (true == $province) {
            /* Province. */
            $form->add('province', 'entity', array(
                'class' => 'Haru\\ProvinceBundle\\Entity\\Province',
                'choice_label' => 'name',
                'required' => false,
                'empty_value' => 'form.empty_value',
                'label' => 'haru.city.city.province',
            ));
        }

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getName()
    {
        return 'haru_city_city';
    }

}
