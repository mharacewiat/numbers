<?php

namespace Haru\CityBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition,
    Symfony\Component\Config\Definition\Builder\TreeBuilder,
    Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration class.
 *
 * @package Haru\CityBundle\DependencyInjection
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Configuration implements ConfigurationInterface
{

    // ~ Main methods.

    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('haru_city');

        // ~

        /* Prepare configuration. */
        $this->prepareConfiguration($rootNode);

        // ~

        return $treeBuilder;
    }

    // ~ Helpers.

    /**
     * @param NodeDefinition $rootNode
     * @return $this
     */
    protected function prepareConfiguration(NodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->scalarNode('class')
            ->end();

        // ~

        return $this;
    }

}
