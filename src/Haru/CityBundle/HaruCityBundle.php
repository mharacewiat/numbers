<?php

namespace Haru\CityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HaruCityBundle class.
 *
 * @package Haru\CityBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruCityBundle extends Bundle
{

}
