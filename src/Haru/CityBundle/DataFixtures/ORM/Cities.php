<?php

namespace Haru\CityBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\CityBundle\Entity\City,
    Haru\CityBundle\Service\Manager as CityManager,
    Haru\Common\DataFixture;

/**
 * Cities class.
 *
 * @package Haru\CityBundle\DataFixtures\ORM
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Cities extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load cities data. */
        $citiesData = $this->loadCitiesData();

        // ~

        /* Load cities. */
        $this->loadCities($citiesData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $citiesData
     * @param ObjectManager $manager
     */
    protected function loadCities(array $citiesData, ObjectManager $manager)
    {
        /* Iterate through cities data. */
        foreach ($citiesData as $key => $value) {
            /* Load city. */
            $this->loadCity($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $cityData
     * @param ObjectManager $manager
     * @return City
     */
    protected function loadCity($key, array $cityData, ObjectManager $manager)
    {
        unset($key);

        // ~

        /* Build city. */
        $city = $this->buildCity($cityData, $manager);

        // ~

        return $city;
    }

    // ~

    /**
     * @param array $cityData
     * @param ObjectManager $manager
     * @return City
     */
    protected function buildCity(array $cityData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $cityManager = $this->getCityManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $name
         * @var string $province
         */
        list($name, $province) = $this->prepareData($cityData);

        // ~

        /* Get clean province reference. */
        if (
            (0 === strpos($province, '@')) &&
            (true == $this->hasReference($provinceKey = substr($province, 1)))
        ) {
            $province = $this->getReference($provinceKey);
        } else {
            dump($province);exit;
            $province = null;
        }

        // ~

        $city = $cityManager->create();

        // ~

        $city->setName($name);
        $city->setProvince($province);

        // ~

        $cityManager->update($city, false);

        // ~

        /* Store city. */
        $this->storeCity($name, $city);

        // ~

        return $city;
    }

    // ~

    /**
     * @param $key
     * @param City $city
     * @return $this
     */
    protected function storeCity($key, City $city)
    {
        $this->setReference($key, $city);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadCitiesData()
    {
        return $this->loadData('cities');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('name', 'province'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return CityManager
     */
    protected function getCityManager()
    {
        return $this->getContainer()->get('haru_city.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 301;
    }

}
