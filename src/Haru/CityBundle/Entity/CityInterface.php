<?php

namespace Haru\CityBundle\Entity;

use Haru\ORM\Mapping\EntityInterface,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * CityInterface interface.
 *
 * @package Haru\CityBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CityInterface extends EntityInterface
{

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CityInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CityInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return CityInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

    // ~

    /**
     * Set province
     *
     * @param ProvinceInterface $province
     *
     * @return CityInterface
     */
    public function setProvince(ProvinceInterface $province = null);

    /**
     * Get province
     *
     * @return ProvinceInterface
     */
    public function getProvince();

}
