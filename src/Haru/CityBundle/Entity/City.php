<?php

namespace Haru\CityBundle\Entity;

use Haru\ORM\Mapping\Entity,
    Haru\ProvinceBundle\Entity\ProvinceInterface;

/**
 * City class.
 *
 * @package Haru\CityBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class City extends Entity implements CityInterface
{

    // ~ Properties.

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $deleted_at;

    // ~

    /**
     * @var ProvinceInterface
     */
    protected $province;

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CityInterface
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CityInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return CityInterface
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    // ~

    /**
     * Set province
     *
     * @param ProvinceInterface $province
     *
     * @return CityInterface
     */
    public function setProvince(ProvinceInterface $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return ProvinceInterface
     */
    public function getProvince()
    {
        return $this->province;
    }

}
