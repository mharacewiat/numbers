<?php

namespace Haru\CityBundle\Event;

/**
 * FilterCityResponseEventAbstract abstract class.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterCityResponseEventAbstract extends CityEventAbstract implements FilterCityResponseEventInterface
{

}
