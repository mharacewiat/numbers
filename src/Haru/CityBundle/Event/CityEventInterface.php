<?php

namespace Haru\CityBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    Haru\CityBundle\Entity\CityInterface;

/**
 * CityEventInterface interface.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CityEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return CityInterface|null
     */
    public function getCity();

}
