<?php

namespace Haru\CityBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetCityResponseEventInterface interface.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetCityResponseEventInterface extends CityEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'haru_city.city.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'haru_city.city.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'haru_city.city.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
