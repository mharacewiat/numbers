<?php

namespace Haru\CityBundle\Event;

/**
 * FilterCityResponseEventInterface interface.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterCityResponseEventInterface extends CityEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'haru_city.city.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'haru_city.city.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'haru_city.city.delete.completed';

}
