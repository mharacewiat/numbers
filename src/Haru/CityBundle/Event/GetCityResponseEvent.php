<?php

namespace Haru\CityBundle\Event;

/**
 * GetCityResponseEvent class.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetCityResponseEvent extends GetCityResponseEventAbstract
{

}
