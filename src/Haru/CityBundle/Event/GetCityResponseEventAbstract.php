<?php

namespace Haru\CityBundle\Event;

use Haru\CityBundle\Entity\CityInterface,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GetCityResponseEventAbstract abstract class.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GetCityResponseEventAbstract extends CityEvent implements GetCityResponseEventInterface
{

    // ~ Properties.

    /**
     * @var Response|null
     */
    protected $response = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    // ~ Magic methods.

    /**
     * GetCityResponseEventAbstract constructor.
     *
     * @param CityInterface|null $city
     * @param Request|null $request
     */
    public function __construct(CityInterface $city = null, Request $request = null)
    {
        parent::__construct($city);

        // ~

        $this->setRequest($request);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     * @return $this
     */
    protected function setResponse(Response $response = null)
    {
        $this->response = $response;
        return $this;
    }

    // ~

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     * @return $this
     */
    protected function setRequest(Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

}
