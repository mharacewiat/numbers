<?php

namespace Haru\CityBundle\Event;

use Haru\Component\EventDispatcher\Event,
    Haru\CityBundle\Entity\CityInterface;

/**
 * CityEventAbstract abstract class.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class CityEventAbstract extends Event implements CityEventInterface
{

    // ~ Properties.

    /**
     * @var CityInterface|null
     */
    protected $city = null;

    // ~ Magic methods.

    /**
     * CityEventAbstract constructor.
     *
     * @param CityInterface|null $city
     */
    public function __construct(CityInterface $city = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setCity($city);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return CityInterface|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param CityInterface|null $city
     * @return $this
     */
    protected function setCity(CityInterface $city = null)
    {
        $this->city = $city;
        return $this;
    }

}
