<?php

namespace Haru\CityBundle\Event;

/**
 * FilterCityResponseEvent class.
 *
 * @package Haru\CityBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterCityResponseEvent extends FilterCityResponseEventAbstract
{

}
