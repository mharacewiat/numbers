<?php

namespace Haru\CityBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * CityRepository class.
 *
 * @package Haru\CityBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CityRepository extends EntityRepository
{

}
