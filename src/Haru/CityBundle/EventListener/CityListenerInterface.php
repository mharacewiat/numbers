<?php

namespace Haru\CityBundle\EventListener;

use Haru\CityBundle\Event\FilterCityResponseEventInterface,
    Haru\CityBundle\Event\FormEventInterface,
    Haru\CityBundle\Event\GetCityResponseEventInterface,
    Haru\Component\EventDispatcher\EventListenerInterface;

/**
 * CityListenerInterface interface.
 *
 * @package Haru\CityBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface CityListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetCityResponseEventInterface $event
     */
    public function onListInitialize(GetCityResponseEventInterface $event);

    /**
     * @param FilterCityResponseEventInterface $event
     */
    public function onListCompleted(FilterCityResponseEventInterface $event);

    // ~

    /**
     * @param GetCityResponseEventInterface $event
     */
    public function onEditInitialize(GetCityResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterCityResponseEventInterface $event
     */
    public function onEditCompleted(FilterCityResponseEventInterface $event);

    // ~

    /**
     * @param GetCityResponseEventInterface $event
     */
    public function onDeleteInitialize(GetCityResponseEventInterface $event);

    /**
     * @param FilterCityResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterCityResponseEventInterface $event);

}
