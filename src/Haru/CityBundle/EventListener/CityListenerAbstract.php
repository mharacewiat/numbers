<?php

namespace Haru\CityBundle\EventListener;

use Haru\CityBundle\Event\FilterCityResponseEventInterface,
    Haru\CityBundle\Event\FormEventInterface,
    Haru\CityBundle\Event\GetCityResponseEventInterface,
    Haru\Component\EventDispatcher\EventListener;

/**
 * CityListenerAbstract abstract class.
 *
 * @package Haru\CityBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class CityListenerAbstract extends EventListener implements CityListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetCityResponseEventInterface $event
     */
    abstract public function onListInitialize(GetCityResponseEventInterface $event);

    /**
     * @param FilterCityResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterCityResponseEventInterface $event);

    // ~

    /**
     * @param GetCityResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetCityResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterCityResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterCityResponseEventInterface $event);

    // ~

    /**
     * @param GetCityResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetCityResponseEventInterface $event);

    /**
     * @param FilterCityResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterCityResponseEventInterface $event);

}
