<?php

namespace Haru\CityBundle\EventListener;

use Haru\CityBundle\Event\FilterCityResponseEventInterface,
    Haru\CityBundle\Event\FormEventInterface,
    Haru\CityBundle\Event\GetCityResponseEventInterface;

/**
 * CityListener class.
 *
 * @package Haru\CityBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CityListener extends CityListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetCityResponseEventInterface $event
     */
    public function onListInitialize(GetCityResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterCityResponseEventInterface $event
     */
    public function onListCompleted(FilterCityResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetCityResponseEventInterface $event
     */
    public function onEditInitialize(GetCityResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterCityResponseEventInterface $event
     */
    public function onEditCompleted(FilterCityResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'city.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetCityResponseEventInterface $event
     */
    public function onDeleteInitialize(GetCityResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterCityResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterCityResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'city.delete');

        // ~

        return;
    }

}
