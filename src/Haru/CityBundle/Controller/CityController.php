<?php

namespace Haru\CityBundle\Controller;

use Doctrine\DBAL\DBALException,
    Haru\CityBundle\Entity\City,
    Haru\CityBundle\Event\FilterCityResponseEvent,
    Haru\CityBundle\Event\FilterCityResponseEventInterface,
    Haru\CityBundle\Event\FormEvent,
    Haru\CityBundle\Event\FormEventInterface,
    Haru\CityBundle\Event\GetCityResponseEvent,
    Haru\CityBundle\Form\CityType,
    Haru\CityBundle\Repository\CityRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * CityController class.
 *
 * @package Haru\CityBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class CityController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to city list action. */
        return $this->redirectToRoute('haru_city_city_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'cities' => null,
        );

        // ~

        /* Prepare services. */

        $cityManager = $this->getCityManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetCityResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetCityResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var CityRepository $cityRepository */
        $cityRepository = $cityManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all cities. */
        $cities = $cityRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['cities'] = $cities;

        // ~

        $response = $this->render('HaruCityBundle:City:list.html.twig', $renderData);

        // ~

        $event = new FilterCityResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterCityResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'city' => null,
        );

        // ~

        /* Prepare services. */

        $cityManager = $this->getCityManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var CityRepository $cityRepository */
        $cityRepository = $cityManager->getRepository();

        /** @var City $city */
        $city = $cityRepository->find($id);

        // ~

        if (true == is_null($city)) {
            /** @var City $city */
            $city = $cityManager->create();
        }

        // ~

        $event = new GetCityResponseEvent($city, $request);
        $eventDispatcher->dispatch(GetCityResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new CityType();
        $formOptions = array();

        $form = $this->createForm($formType, $city, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update city. */
                    $cityManager->update($city, true);

                    // ~

                    $event = new FilterCityResponseEvent($city, $response);
                    $eventDispatcher->dispatch(FilterCityResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('haru_city_city_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['city'] = $city;

        // ~

        return $this->render('HaruCityBundle:City:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $cityManager = $this->getCityManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var CityRepository $cityRepository */
        $cityRepository = $cityManager->getRepository();

        /** @var City $city */
        $city = $cityRepository->find($id);

        // ~

        $event = new GetCityResponseEvent($city, $request);
        $eventDispatcher->dispatch(GetCityResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($city)) {
            try {
                /* Update city. */
                $cityManager->delete($city, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('haru_city_city_list', array());

        // ~

        $event = new FilterCityResponseEvent($city, $response);
        $eventDispatcher->dispatch(FilterCityResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
