<?php

namespace Haru\CityBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    Haru\CityBundle\Service\Manager as CityManager;

/**
 * Controller abstract class.
 *
 * @package Haru\CityBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return CityManager
     */
    protected function getCityManager()
    {
        return $this->getContainer()->get('haru_city.manager');
    }

}
