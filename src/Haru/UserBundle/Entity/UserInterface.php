<?php

namespace Haru\UserBundle\Entity;

use FOS\UserBundle\Model\UserInterface as BaseUserInterface;

/**
 * UserInterface interface.
 *
 * @package Haru\UserBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface UserInterface extends BaseUserInterface
{

    // ~ Getters and setters.

    /**
     * Set name
     *
     * @param string $forename
     *
     * @return UserInterface
     */
    public function setForename($forename);

    /**
     * Get name
     *
     * @return string
     */
    public function getForename();

    /**
     * Set name
     *
     * @param string $surname
     *
     * @return UserInterface
     */
    public function setSurname($surname);

    /**
     * Get name
     *
     * @return string
     */
    public function getSurname();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UserInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return UserInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

}
