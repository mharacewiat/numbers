<?php

namespace Haru\UserBundle\Entity;

use Doctrine\Common\Collections\Collection,
    FOS\UserBundle\Model\GroupInterface as BaseGroupInterface,
    FOS\UserBundle\Model\UserInterface as BaseUserInterface;

/**
 * GroupInterface interface.
 *
 * @package Haru\UserBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GroupInterface extends BaseGroupInterface
{

    // Getters and setters.

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return GroupInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return GroupInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

    // ~

    /**
     * Add user
     *
     * @param BaseUserInterface $user
     *
     * @return GroupInterface
     */
    public function addUser(BaseUserInterface $user);

    /**
     * Remove user
     *
     * @param BaseUserInterface $user
     */
    public function removeUser(BaseUserInterface $user);

    /**
     * Get users
     *
     * @return Collection
     */
    public function getUsers();

}
