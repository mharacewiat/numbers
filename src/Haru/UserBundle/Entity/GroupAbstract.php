<?php

namespace Haru\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection,
    Doctrine\Common\Collections\Collection,
    FOS\UserBundle\Model\UserInterface as BaseUserInterface,
    Haru\ORM\Mapping\Entity;

/**
 * GroupAbstract abstract class.
 *
 * @package Haru\UserBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GroupAbstract extends Entity implements GroupInterface
{

    // ~ Properties.

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $deleted_at;

    // ~

    /**
     * @var Collection
     */
    protected $users;

    /**
     * @var Collection
     */
    protected $roles;

    // ~ Magic methods

    /**
     * GroupAbstract constructor.
     *
     * @param $name
     */
    public function __construct($name)
    {
        //parent::__construct();

        // ~

        $this->setName($name);

        // ~

        $this->users = new ArrayCollection();
        $this->roles = new ArrayCollection();

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->getRoles()->contains($role);
    }

    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Group
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Group
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    // ~

    /**
     * Add user
     *
     * @param BaseUserInterface $user
     *
     * @return Group
     */
    public function addUser(BaseUserInterface $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param BaseUserInterface $user
     */
    public function removeUser(BaseUserInterface $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        foreach ($roles as $key => $value) {
            $this->addRole($value);
        }

        // ~

        return $this;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function addRole($role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function removeRole($role)
    {
        $this->roles->removeElement($role);

        return $this;
    }

    /**
     * Get roles
     *
     * @return Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

}
