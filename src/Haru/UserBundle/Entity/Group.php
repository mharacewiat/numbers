<?php

namespace Haru\UserBundle\Entity;

use Haru\RoleBundle\Entity\RoleInterface;

/**
 * Group class.
 *
 * @package Haru\UserBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Group extends GroupAbstract
{

    // ~ Getters and setters.

    /**
     * @param string $role
     * @return $this
     */
    public function addRole($role)
    {
        if (false == ($role instanceof RoleInterface)) {
            throw new \InvalidargumentException();
        }

        // ~

        return parent::addRole($role);
    }

    /**
     * @param string $role
     * @return $this
     */
    public function removeRole($role)
    {
        if (false == ($role instanceof RoleInterface)) {
            throw new \InvalidargumentException();
        }

        // ~

        return parent::removeRole($role);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return array_map('strval', iterator_to_array(parent::getRoles()));
    }

}
