<?php

namespace Haru\UserBundle\Form;

use Haru\Component\Form\AbstractType;

/**
 * RegistrationFormType class.
 *
 * @package Haru\UserBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class RegistrationFormType extends AbstractType
{

    // ~ Main methods.

    // @todo

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getParent()
    {
        return 'FOS\\UserBundle\\Form\\Type\\RegistrationFormType';
    }

    // ~

    /**
     * @return string
     */
    public function getName()
    {
        return 'haru_user_registration';
    }

}
