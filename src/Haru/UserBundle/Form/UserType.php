<?php

namespace Haru\UserBundle\Form;

use Haru\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\Form\FormEvent,
    Symfony\Component\Form\FormEvents,
    Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * UserType class.
 *
 * @package Haru\UserBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class UserType extends AbstractType
{

    // ~ Main methods.

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* Build parent form. */
        parent::buildForm($builder, $options);

        // ~

        /* Username. */
        $builder->add('username', null, array(
            'label' => 'haru.user.user.username',
        ));

        /* Password. */
        $builder->add('password', 'password', array(
            'label' => 'haru.user.user.password',
        ));

        /* Email. */
        $builder->add('email', null, array(
            'label' => 'haru.user.user.email',
        ));

        /* Forename. */
        $builder->add('forename', null, array(
            'label' => 'haru.user.user.forename',
        ));

        /* Surname. */
        $builder->add('surname', null, array(
            'label' => 'haru.user.user.surname',
        ));

        ///* Groups. */
        //$builder->add('groups', 'collection', array(
        //    'label' => 'haru.user.user.groups',
        //));

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param FormBuilderInterface $builder
     */
    protected function registerListeners(FormBuilderInterface $builder)
    {
        /* Register parent listeners. */
        parent::registerListeners($builder);

        // ~

        /* Register created at event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'createdAtListener'), 0);

        /* Register deleted at event listener. */
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'deletedAtListener'), 0);

        // ~

        return;
    }

    // ~

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /* Configure parent options. */
        parent::configureOptions($resolver);

        // ~

        $resolver->setDefaults(array(
            'data_class' => 'Haru\\UserBundle\\Entity\\User',
            // ~
            'created_at' => false,
            'deleted_at' => false,
            // ~
            'province' => false,
            // ~
            'submit' => true,
        ));

        // ~

        $resolver->addAllowedTypes('created_at', array(
            'bool',
            'int',
        ));

        $resolver->addAllowedTypes('deleted_at', array(
            'bool',
            'int',
        ));

        $resolver->addAllowedTypes('province', array(
            'bool',
            'int',
        ));

        // ~

        return;
    }

    // ~ Events.

    /**
     * @param FormEvent $event
     */
    public function createdAtListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $createdAt */
        $createdAt = $form->getConfig()->getOption('created_at');

        // ~

        if (true == $createdAt) {
            /* Created at. */
            $form->add('created_at', 'datetime', array(
                'label' => 'haru.user.user.created_at',
            ));
        }

        // ~

        return;
    }

    /**
     * @param FormEvent $event
     */
    public function deletedAtListener(FormEvent $event)
    {
        $form = $event->getForm();


        /** @var bool|int $deletedAt */
        $deletedAt = $form->getConfig()->getOption('deleted_at');

        // ~

        if (true == $deletedAt) {
            /* Deleted at. */
            $form->add('deleted_at', 'datetime', array(
                'label' => 'haru.user.user.deleted_at',
            ));
        }

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getName()
    {
        return 'haru_user_user';
    }

}
