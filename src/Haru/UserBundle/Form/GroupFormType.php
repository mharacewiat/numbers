<?php

namespace Haru\UserBundle\Form;

use Haru\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface;

/**
 * GroupFormType class.
 *
 * @package Haru\UserBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GroupFormType extends AbstractType
{

    // ~ Main methods.

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* Build parent form. */
        parent::buildForm($builder, $options);

        // ~

        /* Roles. */
        $builder->add('roles', 'entity', array(
            'class' => 'Haru\\RoleBundle\\Entity\\Role',
            'choice_label' => 'name',
            'multiple' => true,
            'required' => false,
            'empty_value' => 'form.empty_value',
            'label' => 'haru.user.group.roles',
        ));

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getParent()
    {
        return 'FOS\\UserBundle\\Form\\Type\\GroupFormType';
    }

    // ~

    /**
     * @return string
     */
    public function getName()
    {
        return 'haru_user_group';
    }

}
