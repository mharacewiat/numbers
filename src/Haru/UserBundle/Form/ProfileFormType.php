<?php

namespace Haru\UserBundle\Form;

use Haru\Component\Form\AbstractType;

/**
 * ProfileFormType class.
 *
 * @package Haru\UserBundle\Form
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProfileFormType extends AbstractType
{

    // ~ Main methods.

    // @todo

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getParent()
    {
        return 'FOS\\UserBundle\\Form\\Type\\ProfileFormType';
    }

    // ~

    /**
     * @return string
     */
    public function getName()
    {
        return 'haru_user_profile';
    }

}
