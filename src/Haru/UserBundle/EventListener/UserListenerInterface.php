<?php

namespace Haru\UserBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    Haru\UserBundle\Event\FilterUserResponseEventInterface,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetUserResponseEventInterface;

/**
 * UserListenerInterface interface.
 *
 * @package Haru\UserBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface UserListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetUserResponseEventInterface $event
     */
    public function onListInitialize(GetUserResponseEventInterface $event);

    /**
     * @param FilterUserResponseEventInterface $event
     */
    public function onListCompleted(FilterUserResponseEventInterface $event);

    // ~

    /**
     * @param GetUserResponseEventInterface $event
     */
    public function onEditInitialize(GetUserResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterUserResponseEventInterface $event
     */
    public function onEditCompleted(FilterUserResponseEventInterface $event);

    // ~

    /**
     * @param GetUserResponseEventInterface $event
     */
    public function onDeleteInitialize(GetUserResponseEventInterface $event);

    /**
     * @param FilterUserResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterUserResponseEventInterface $event);

}
