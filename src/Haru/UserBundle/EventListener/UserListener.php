<?php

namespace Haru\UserBundle\EventListener;

use Haru\UserBundle\Event\FilterUserResponseEventInterface,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetUserResponseEventInterface;

/**
 * UserListener class.
 *
 * @package Haru\UserBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class UserListener extends UserListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetUserResponseEventInterface $event
     */
    public function onListInitialize(GetUserResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterUserResponseEventInterface $event
     */
    public function onListCompleted(FilterUserResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetUserResponseEventInterface $event
     */
    public function onEditInitialize(GetUserResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterUserResponseEventInterface $event
     */
    public function onEditCompleted(FilterUserResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'user.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetUserResponseEventInterface $event
     */
    public function onDeleteInitialize(GetUserResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterUserResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterUserResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'user.delete');

        // ~

        return;
    }

}
