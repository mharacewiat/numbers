<?php

namespace Haru\UserBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    Haru\UserBundle\Event\FilterGroupResponseEventInterface,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetGroupResponseEventInterface;

/**
 * GroupListenerInterface interface.
 *
 * @package Haru\UserBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GroupListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetGroupResponseEventInterface $event
     */
    public function onListInitialize(GetGroupResponseEventInterface $event);

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    public function onListCompleted(FilterGroupResponseEventInterface $event);

    // ~

    /**
     * @param GetGroupResponseEventInterface $event
     */
    public function onEditInitialize(GetGroupResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    public function onEditCompleted(FilterGroupResponseEventInterface $event);

    // ~

    /**
     * @param GetGroupResponseEventInterface $event
     */
    public function onDeleteInitialize(GetGroupResponseEventInterface $event);

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterGroupResponseEventInterface $event);

}
