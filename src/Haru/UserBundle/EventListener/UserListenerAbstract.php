<?php

namespace Haru\UserBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    Haru\UserBundle\Event\FilterUserResponseEventInterface,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetUserResponseEventInterface;

/**
 * UserListenerAbstract abstract class.
 *
 * @package Haru\UserBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class UserListenerAbstract extends EventListener implements UserListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetUserResponseEventInterface $event
     */
    abstract public function onListInitialize(GetUserResponseEventInterface $event);

    /**
     * @param FilterUserResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterUserResponseEventInterface $event);

    // ~

    /**
     * @param GetUserResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetUserResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterUserResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterUserResponseEventInterface $event);

    // ~

    /**
     * @param GetUserResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetUserResponseEventInterface $event);

    /**
     * @param FilterUserResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterUserResponseEventInterface $event);

}
