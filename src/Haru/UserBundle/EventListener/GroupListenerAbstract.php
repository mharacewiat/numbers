<?php

namespace Haru\UserBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    Haru\UserBundle\Event\FilterGroupResponseEventInterface,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetGroupResponseEventInterface;

/**
 * GroupListenerAbstract abstract class.
 *
 * @package Haru\UserBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GroupListenerAbstract extends EventListener implements GroupListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetGroupResponseEventInterface $event
     */
    abstract public function onListInitialize(GetGroupResponseEventInterface $event);

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterGroupResponseEventInterface $event);

    // ~

    /**
     * @param GetGroupResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetGroupResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterGroupResponseEventInterface $event);

    // ~

    /**
     * @param GetGroupResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetGroupResponseEventInterface $event);

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterGroupResponseEventInterface $event);

}
