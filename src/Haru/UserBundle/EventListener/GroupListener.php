<?php

namespace Haru\UserBundle\EventListener;

use Haru\UserBundle\Event\FilterGroupResponseEventInterface,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetGroupResponseEventInterface;

/**
 * GroupListener class.
 *
 * @package Haru\UserBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GroupListener extends GroupListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetGroupResponseEventInterface $event
     */
    public function onListInitialize(GetGroupResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    public function onListCompleted(FilterGroupResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetGroupResponseEventInterface $event
     */
    public function onEditInitialize(GetGroupResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    public function onEditCompleted(FilterGroupResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'group.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetGroupResponseEventInterface $event
     */
    public function onDeleteInitialize(GetGroupResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterGroupResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterGroupResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'group.delete');

        // ~

        return;
    }

}
