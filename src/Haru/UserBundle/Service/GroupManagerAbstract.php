<?php

namespace Haru\UserBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    FOS\UserBundle\Doctrine\GroupManager;

/**
 * ManagerAbstract abstract class.
 *
 * @package Haru\UserBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GroupManagerAbstract extends GroupManager implements GroupManagerInterface
{

    // ~ Main methods.

    /**
     * Repository getter.
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

}
