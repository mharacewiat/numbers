<?php

namespace Haru\UserBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    FOS\UserBundle\Model\GroupManagerInterface as BaseGroupManagerInterface;

/**
 * ManagerInterface interface.
 *
 * @package Haru\UserBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GroupManagerInterface extends BaseGroupManagerInterface
{

    // ~ Main methods.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

}
