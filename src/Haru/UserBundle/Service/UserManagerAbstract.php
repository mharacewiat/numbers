<?php

namespace Haru\UserBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    FOS\UserBundle\Doctrine\UserManager;

/**
 * ManagerAbstract abstract class.
 *
 * @package Haru\UserBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class UserManagerAbstract extends UserManager implements UserManagerInterface
{

    // ~ Main methods.

    /**
     * Repository getter.
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

}
