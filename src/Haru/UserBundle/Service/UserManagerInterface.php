<?php

namespace Haru\UserBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    FOS\UserBundle\Model\UserManagerInterface as BaseUserManagerInterface;

/**
 * ManagerInterface interface.
 *
 * @package Haru\UserBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface UserManagerInterface extends BaseUserManagerInterface
{

    // ~ Main methods.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

}
