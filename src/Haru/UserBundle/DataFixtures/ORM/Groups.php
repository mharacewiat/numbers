<?php

namespace Haru\UserBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager,
    FOS\UserBundle\Doctrine\GroupManager,
    FOS\UserBundle\Model\GroupInterface,
    Haru\Common\DataFixture,
    Haru\RoleBundle\Entity\Role,
    Haru\UserBundle\Entity\Group;

/**
 * Groups class.
 *
 * @package Haru\UserBundle\DataFixtures\ORM
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Groups extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load groups data. */
        $groupsData = $this->loadGroupsData();

        // ~

        /* Load groups. */
        $this->loadGroups($groupsData, $manager);

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $groupsData
     * @param ObjectManager $manager
     * @return $this
     */
    protected function loadGroups(array $groupsData, ObjectManager $manager)
    {
        /* Iterate through groups data. */
        foreach ($groupsData as $key => $value) {
            /* Load group. */
            $this->loadGroup($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return $this;
    }

    /**
     * @param $key
     * @param array $groupData
     * @param ObjectManager $manager
     * @return GroupInterface
     */
    protected function loadGroup($key, array $groupData, ObjectManager $manager)
    {
        //unset($key);
        //
        //// ~

        /* Build group. */
        $group = $this->buildGroup($groupData, $manager);

        // ~

        /* Store group. */
        $this->storeGroup($key, $group);

        // ~

        return $group;
    }

    // ~

    /**
     * @param array $groupData
     * @param ObjectManager $manager
     * @return GroupInterface
     */
    protected function buildGroup(array $groupData, ObjectManager $manager)
    {
        //unset($manager);
        //
        //// ~

        $groupManager = $this->getGroupManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $name
         * @var array $rolesData
         */
        list(
            $name,
            $rolesData
            ) = $this->prepareData($groupData);

        // ~

        /* Create group. */

        /** @var Group $group */
        $group = $groupManager->createGroup($name);

        // ~

        if (false == is_null($rolesData)) {
            /* Handle roles. */
            $this->handleRoles($group, $rolesData, $manager);
        }

        // ~

        /* Persist group. */
        $groupManager->updateGroup($group, false);

        // ~

        return $group;
    }

    // ~


    /**
     * @param Group $group
     * @param array $rolesData
     * @param ObjectManager $manager
     * @return $this
     */
    protected function handleRoles(Group $group, array $rolesData, ObjectManager $manager)
    {
        foreach ($rolesData as $key => $value) {
            try {
                /* Handle role. */
                $this->handleRole($group, $value, $manager);
            } catch (\Exception $exception) {
                /* Do nothing. */
            }
        }

        // ~

        return $this;
    }

    /**
     * @param Group $group
     * @param $roleData
     * @param ObjectManager $manager
     * @return Role|null
     */
    protected function handleRole(Group $group, $roleData, ObjectManager $manager)
    {
        /** @var Role $role */
        $role = null;

        // ~

        /* Get clean role reference. */
        if (
            (0 === strpos($roleData, '@')) &&
            (true == $this->hasReference($roleKey = substr($roleData, 1)))
        ) {
            $role = $this->getReference($roleKey);
        }

        // ~

        if (false == is_null($role)) {
            $manager->persist($role);

            // ~

            $group->addRole($role);
            $role->addGroup($group);
        }

        // ~

        return $role;
    }

    // ~

    /**
     * @param $key
     * @param GroupInterface $group
     * @return $this
     */
    protected function storeGroup($key, GroupInterface $group)
    {
        $this->setReference(vsprintf('group-%1$s', array($key)), $group);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadGroupsData()
    {
        return $this->loadData('groups');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('name'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return GroupManager
     */
    protected function getGroupManager()
    {
        return $this->getContainer()->get('fos_user.group_manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 101;
    }

}
