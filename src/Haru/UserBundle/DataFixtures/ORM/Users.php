<?php

namespace Haru\UserBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager,
    FOS\UserBundle\Doctrine\UserManager,
    Haru\Common\DataFixture,
    Haru\UserBundle\Entity\Group,
    Haru\UserBundle\Entity\User;

/**
 * Users class.
 *
 * @package Haru\UserBundle\DataFixtures\ORM
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Users extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load users data. */
        $usersData = $this->loadUsersData();

        // ~

        /* Load users. */
        $this->loadUsers($usersData, $manager);

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $usersData
     * @param ObjectManager $manager
     * @return $this
     */
    protected function loadUsers(array $usersData, ObjectManager $manager)
    {
        /* Iterate through users data. */
        foreach ($usersData as $key => $value) {
            /* Load user. */
            $this->loadUser($key, $value, $manager);
        }

        // ~

        return $this;
    }

    /**
     * @param $key
     * @param array $userData
     * @param ObjectManager $manager
     * @return User
     */
    protected function loadUser($key, array $userData, ObjectManager $manager)
    {
        unset($key);

        // ~

        /* Build user. */
        $user = $this->buildUser($userData, $manager);

        // ~

        return $user;
    }

    // ~

    /**
     * @param array $userData
     * @param ObjectManager $manager
     * @return User
     */
    protected function buildUser(array $userData, ObjectManager $manager)
    {
        //unset($manager);
        //
        //// ~

        $userManager = $this->getUserManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $username
         * @var string $plainPassword
         * @var string $email
         * @var array $groupsData
         */
        list(
            $username,
            $plainPassword,
            $email,
            $groupsData
            ) = $this->prepareData($userData);

        // ~

        /* Create user. */

        /** @var User $user */
        $user = $userManager->createUser();

        // ~

        $user->setEnabled(true);
        $user->setUsername($username);
        $user->setPlainPassword($plainPassword);
        $user->setEmail($email);

        // ~

        if (false == is_null($groupsData)) {
            /* Handle groups. */
            $this->handleGroups($user, $groupsData, $manager);
        }

        // ~

        /* Persist user. */
        $userManager->updateUser($user, false);

        // ~

        /* Store user. */
        $this->storeUser($username, $user);

        // ~

        return $user;
    }

    // ~

    /**
     * @param User $user
     * @param array $groupsData
     * @param ObjectManager $manager
     * @return $this
     */
    protected function handleGroups(User $user, array $groupsData, ObjectManager $manager)
    {
        foreach ($groupsData as $key => $value) {
            try {
                /* Handle group. */
                $this->handleGroup($user, $value, $manager);
            } catch (\Exception $exception) {
                /* Do nothing. */
            }
        }

        // ~

        return $this;
    }

    /**
     * @param User $user
     * @param $groupData
     * @param ObjectManager $manager
     * @return Group|null
     */
    protected function handleGroup(User $user, $groupData, ObjectManager $manager)
    {
        /** @var Group $group */
        $group = null;

        // ~

        /* Get clean group reference. */
        if (
            (0 === strpos($groupData, '@')) &&
            (true == $this->hasReference($groupKey = substr($groupData, 1)))
        ) {
            $group = $this->getReference($groupKey);
        }

        // ~

        if (false == is_null($group)) {
            $manager->persist($group);

            // ~

            $user->addGroup($group);
            $group->addUser($user);
        }

        // ~

        return $group;
    }

    // ~

    /**
     * @param $key
     * @param User $user
     * @return $this
     */
    protected function storeUser($key, User $user)
    {
        $this->setReference(vsprintf('user-%1$s', array($key)), $user);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadUsersData()
    {
        return $this->loadData('users');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('username', 'plainPassword', 'email', 'groups'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return UserManager
     */
    protected function getUserManager()
    {
        return $this->getContainer()->get('fos_user.user_manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 201;
    }

}
