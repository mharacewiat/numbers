<?php

namespace Haru\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HaruUserBundle class.
 *
 * @package Haru\UserBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruUserBundle extends Bundle
{

    // ~ Getters and setters.

    /**
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }

}
