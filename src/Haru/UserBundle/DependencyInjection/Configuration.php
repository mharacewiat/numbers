<?php

namespace Haru\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder,
    Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration class.
 *
 * @package Haru\UserBundle\DependencyInjection
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Configuration implements ConfigurationInterface
{

    // ~ Main methods.

    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('haru_user');

        // ~

        return $treeBuilder;
    }

}
