<?php

namespace Haru\UserBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator,
    Symfony\Component\DependencyInjection\ContainerBuilder,
    Symfony\Component\DependencyInjection\Loader,
    Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * HaruUserExtension class.
 *
 * @package Haru\UserBundle\DependencyInjection
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class HaruUserExtension extends Extension
{

    // ~ Main methods.

    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        // ~

        return;
    }

}
