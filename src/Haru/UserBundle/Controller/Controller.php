<?php

namespace Haru\UserBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    Haru\UserBundle\Service\GroupManager,
    Haru\UserBundle\Service\UserManager;

/**
 * Controller abstract class.
 *
 * @package Haru\UserBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return GroupManager
     */
    protected function getGroupManager()
    {
        return $this->getContainer()->get('fos_user.group_manager');
    }

    /**
     * @return UserManager
     */
    protected function getUserManager()
    {
        return $this->getContainer()->get('fos_user.user_manager');
    }

}
