<?php

namespace Haru\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * IndexController class.
 *
 * @package Haru\UserBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to user index action. */
        return $this->redirectToRoute('haru_user_user_index', array());
    }

}
