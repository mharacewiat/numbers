<?php

namespace Haru\UserBundle\Controller;

use Doctrine\DBAL\DBALException,
    Haru\UserBundle\Entity\Group,
    Haru\UserBundle\Event\FilterGroupResponseEvent,
    Haru\UserBundle\Event\FilterGroupResponseEventInterface,
    Haru\UserBundle\Event\FormEvent,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetGroupResponseEvent,
    Haru\UserBundle\Form\GroupFormType,
    Haru\UserBundle\Repository\GroupRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GroupController class.
 *
 * @package Haru\UserBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GroupController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to group list action. */
        return $this->redirectToRoute('haru_user_group_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'groups' => null,
        );

        // ~

        /* Prepare services. */

        $groupManager = $this->getGroupManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetGroupResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetGroupResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var GroupRepository $groupRepository */
        $groupRepository = $groupManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all groups. */
        $groups = $groupRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['groups'] = $groups;

        // ~

        $response = $this->render('HaruUserBundle:Group:list.html.twig', $renderData);

        // ~

        $event = new FilterGroupResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterGroupResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'group' => null,
        );

        // ~

        /* Prepare services. */

        $groupManager = $this->getGroupManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var GroupRepository $groupRepository */
        $groupRepository = $groupManager->getRepository();

        /** @var Group $group */
        $group = $groupRepository->find($id);

        // ~

        if (true == is_null($group)) {
            /** @var Group $group */
            $group = $groupManager->createGroup(null);
        }

        // ~

        $event = new GetGroupResponseEvent($group, $request);
        $eventDispatcher->dispatch(GetGroupResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new GroupFormType();
        $formOptions = array();

        $form = $this->createForm($formType, $group, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update group. */
                    $groupManager->updateGroup($group, true);

                    // ~

                    $event = new FilterGroupResponseEvent($group, $response);
                    $eventDispatcher->dispatch(FilterGroupResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('haru_user_group_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['group'] = $group;

        // ~

        return $this->render('HaruUserBundle:Group:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $groupManager = $this->getGroupManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var GroupRepository $groupRepository */
        $groupRepository = $groupManager->getRepository();

        /** @var Group $group */
        $group = $groupRepository->find($id);

        // ~

        $event = new GetGroupResponseEvent($group, $request);
        $eventDispatcher->dispatch(GetGroupResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($group)) {
            try {
                /* Update group. */
                $groupManager->deleteGroup($group);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('haru_user_group_list', array());

        // ~

        $event = new FilterGroupResponseEvent($group, $response);
        $eventDispatcher->dispatch(FilterGroupResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
