<?php

namespace Haru\UserBundle\Controller;

use Doctrine\DBAL\DBALException,
    Haru\UserBundle\Entity\User,
    Haru\UserBundle\Event\FilterUserResponseEvent,
    Haru\UserBundle\Event\FilterUserResponseEventInterface,
    Haru\UserBundle\Event\FormEvent,
    Haru\UserBundle\Event\FormEventInterface,
    Haru\UserBundle\Event\GetUserResponseEvent,
    Haru\UserBundle\Form\UserType,
    Haru\UserBundle\Repository\UserRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * UserController class.
 *
 * @package Haru\UserBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class UserController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to user list action. */
        return $this->redirectToRoute('haru_user_user_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'users' => null,
        );

        // ~

        /* Prepare services. */

        $userManager = $this->getUserManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetUserResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetUserResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var UserRepository $userRepository */
        $userRepository = $userManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all users. */
        $users = $userRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['users'] = $users;

        // ~

        $response = $this->render('HaruUserBundle:User:list.html.twig', $renderData);

        // ~

        $event = new FilterUserResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterUserResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'user' => null,
        );

        // ~

        /* Prepare services. */

        $userManager = $this->getUserManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var UserRepository $userRepository */
        $userRepository = $userManager->getRepository();

        /** @var User $user */
        $user = $userRepository->find($id);

        // ~

        if (true == is_null($user)) {
            /** @var User $user */
            $user = $userManager->createUser();
        }

        // ~

        $event = new GetUserResponseEvent($user, $request);
        $eventDispatcher->dispatch(GetUserResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new UserType();
        $formOptions = array();

        $form = $this->createForm($formType, $user, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update user. */
                    $userManager->updateUser($user, true);

                    // ~

                    $event = new FilterUserResponseEvent($user, $response);
                    $eventDispatcher->dispatch(FilterUserResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('haru_user_user_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['user'] = $user;

        // ~

        return $this->render('HaruUserBundle:User:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $userManager = $this->getUserManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var UserRepository $userRepository */
        $userRepository = $userManager->getRepository();

        /** @var User $user */
        $user = $userRepository->find($id);

        // ~

        $event = new GetUserResponseEvent($user, $request);
        $eventDispatcher->dispatch(GetUserResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($user)) {
            try {
                /* Update user. */
                $userManager->deleteUser($user);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('haru_user_user_list', array());

        // ~

        $event = new FilterUserResponseEvent($user, $response);
        $eventDispatcher->dispatch(FilterUserResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
