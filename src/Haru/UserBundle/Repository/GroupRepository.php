<?php

namespace Haru\UserBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * GroupRepository class.
 *
 * @package Haru\UserBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GroupRepository extends EntityRepository
{

}
