<?php

namespace Haru\UserBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * UserRepository class.
 *
 * @package Haru\UserBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class UserRepository extends EntityRepository
{

}
