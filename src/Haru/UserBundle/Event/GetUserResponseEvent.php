<?php

namespace Haru\UserBundle\Event;

/**
 * GetUserResponseEvent class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetUserResponseEvent extends GetUserResponseEventAbstract
{

}
