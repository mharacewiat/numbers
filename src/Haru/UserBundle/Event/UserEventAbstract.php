<?php

namespace Haru\UserBundle\Event;

use Haru\Component\EventDispatcher\Event,
    Haru\UserBundle\Entity\UserInterface;

/**
 * UserEventAbstract abstract class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class UserEventAbstract extends Event implements UserEventInterface
{

    // ~ Properties.

    /**
     * @var UserInterface|null
     */
    protected $user = null;

    // ~ Magic methods.

    /**
     * UserEventAbstract constructor.
     *
     * @param UserInterface|null $user
     */
    public function __construct(UserInterface $user = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setUser($user);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return UserInterface|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface|null $user
     * @return $this
     */
    protected function setUser(UserInterface $user = null)
    {
        $this->user = $user;
        return $this;
    }

}
