<?php

namespace Haru\UserBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetUserResponseEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetUserResponseEventInterface extends UserEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'haru_user.user.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'haru_user.user.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'haru_user.user.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
