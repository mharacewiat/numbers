<?php

namespace Haru\UserBundle\Event;

/**
 * FilterGroupResponseEvent class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterGroupResponseEvent extends FilterGroupResponseEventAbstract
{

}
