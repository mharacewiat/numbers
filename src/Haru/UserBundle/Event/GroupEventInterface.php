<?php

namespace Haru\UserBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    Haru\UserBundle\Entity\GroupInterface;

/**
 * GroupEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GroupEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return GroupInterface|null
     */
    public function getGroup();

}
