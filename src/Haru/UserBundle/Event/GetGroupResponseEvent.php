<?php

namespace Haru\UserBundle\Event;

/**
 * GetGroupResponseEvent class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetGroupResponseEvent extends GetGroupResponseEventAbstract
{

}
