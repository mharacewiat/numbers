<?php

namespace Haru\UserBundle\Event;

/**
 * FilterUserResponseEventAbstract abstract class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterUserResponseEventAbstract extends UserEventAbstract implements FilterUserResponseEventInterface
{

}
