<?php

namespace Haru\UserBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetGroupResponseEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetGroupResponseEventInterface extends GroupEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'haru_user.group.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'haru_user.group.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'haru_user.group.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
