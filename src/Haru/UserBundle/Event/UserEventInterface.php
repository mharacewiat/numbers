<?php

namespace Haru\UserBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    Haru\UserBundle\Entity\UserInterface;

/**
 * UserEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface UserEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return UserInterface|null
     */
    public function getUser();

}
