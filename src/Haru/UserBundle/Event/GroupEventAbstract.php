<?php

namespace Haru\UserBundle\Event;

use Haru\Component\EventDispatcher\Event,
    Haru\UserBundle\Entity\GroupInterface;

/**
 * GroupEventAbstract abstract class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GroupEventAbstract extends Event implements GroupEventInterface
{

    // ~ Properties.

    /**
     * @var GroupInterface|null
     */
    protected $group = null;

    // ~ Magic methods.

    /**
     * GroupEventAbstract constructor.
     *
     * @param GroupInterface|null $group
     */
    public function __construct(GroupInterface $group = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setGroup($group);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return GroupInterface|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param GroupInterface|null $group
     * @return $this
     */
    protected function setGroup(GroupInterface $group = null)
    {
        $this->group = $group;
        return $this;
    }

}
