<?php

namespace Haru\UserBundle\Event;

/**
 * FilterUserResponseEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterUserResponseEventInterface extends UserEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'haru_user.user.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'haru_user.user.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'haru_user.user.delete.completed';

}
