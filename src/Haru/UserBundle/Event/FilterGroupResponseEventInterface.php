<?php

namespace Haru\UserBundle\Event;

/**
 * FilterGroupResponseEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterGroupResponseEventInterface extends GroupEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'haru_user.group.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'haru_user.group.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'haru_user.group.delete.completed';

}
