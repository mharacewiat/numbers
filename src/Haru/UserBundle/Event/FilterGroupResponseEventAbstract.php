<?php

namespace Haru\UserBundle\Event;

/**
 * FilterGroupResponseEventAbstract abstract class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterGroupResponseEventAbstract extends GroupEventAbstract implements FilterGroupResponseEventInterface
{

}
