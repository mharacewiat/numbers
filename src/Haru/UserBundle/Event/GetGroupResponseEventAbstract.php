<?php

namespace Haru\UserBundle\Event;

use Haru\UserBundle\Entity\GroupInterface,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * GetGroupResponseEventAbstract abstract class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class GetGroupResponseEventAbstract extends GroupEvent implements GetGroupResponseEventInterface
{

    // ~ Properties.

    /**
     * @var Response|null
     */
    protected $response = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    // ~ Magic methods.

    /**
     * GetGroupResponseEventAbstract constructor.
     *
     * @param GroupInterface|null $group
     * @param Request|null $request
     */
    public function __construct(GroupInterface $group = null, Request $request = null)
    {
        /* Construct parent. */
        parent::__construct($group);

        // ~

        $this->setRequest($request);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     * @return $this
     */
    protected function setResponse(Response $response = null)
    {
        $this->response = $response;
        return $this;
    }

    // ~

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     * @return $this
     */
    protected function setRequest(Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

}
