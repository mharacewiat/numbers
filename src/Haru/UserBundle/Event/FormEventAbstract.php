<?php

namespace Haru\UserBundle\Event;

use FOS\UserBundle\Event\FormEvent as BaseFormEvent;

/**
 * FormEventAbstract abstract class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FormEventAbstract extends BaseFormEvent implements FormEventInterface
{

}
