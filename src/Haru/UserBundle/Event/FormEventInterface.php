<?php

namespace Haru\UserBundle\Event;

use Haru\Component\EventDispatcher\FormEventInterface as BaseFormEventInterface;

/**
 * FormEventInterface interface.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FormEventInterface extends BaseFormEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const EDIT_SUCCESS = 'haru_user.user.edit.success';

}
