<?php

namespace Haru\UserBundle\Event;

/**
 * FilterUserResponseEvent class.
 *
 * @package Haru\UserBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterUserResponseEvent extends FilterUserResponseEventAbstract
{

}
