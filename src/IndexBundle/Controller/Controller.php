<?php

namespace IndexBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController;

/**
 * Controller abstract class.
 *
 * @package IndexBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

}
