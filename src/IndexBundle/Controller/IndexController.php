<?php

namespace IndexBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Response;

/**
 * IndexController class.
 *
 * @package IndexBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        return $this->redirectToRoute('index_index_number', array('number' => '+48-123-123-123'));
        //return $this->render('IndexBundle:Index:index.html.twig', array());
    }

    /**
     * @param $number
     * @return Response
     */
    public function numberAction($number)
    {
        return $this->forward('NumberBundle:Number:number', array('number' => $number));
    }

}
