<?php

namespace IndexBundle\Menu;

use Knp\Menu\FactoryInterface,
    Knp\Menu\ItemInterface;

/**
 * Builder class.
 *
 * @package IndexBundle\Menu
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Builder
{

    // ~ Properties.

    /**
     * @var FactoryInterface
     */
    protected $factory;

    // ~ Magic methods.

    /**
     * Builder constructor.
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->setFactory($factory);

        // ~

        return;
    }

    // ~ Main methods.

    /**
     * @param array $options
     * @return ItemInterface
     */
    public function buildMenu(array $options)
    {
        $factory = $this->getFactory();

        // ~

        $root = $factory->createItem('root');

        $city = $root->addChild('route.haru_city_city_index', array('route' => 'haru_city_city_index'));
        //$city->addChild('route.haru_city_city_list', array('route' => 'haru_city_city_list'));

        $country = $root->addChild('route.haru_country_country_index', array('route' => 'haru_country_country_index'));
        //$country->addChild('route.haru_country_country_list', array('route' => 'haru_country_country_list'));

        $province = $root->addChild('route.haru_province_province_index', array('route' => 'haru_province_province_index'));
        //$province->addChild('route.haru_province_province_list', array('route' => 'haru_province_province_list'));

        $role = $root->addChild('route.haru_role_role_index', array('route' => 'haru_role_role_index'));
        //$role->addChild('route.haru_role_role_list', array('route' => 'haru_role_role_list'));

        $group = $root->addChild('route.haru_user_group_index', array('route' => 'haru_user_group_index'));
        //$group->addChild('route.haru_user_group_list', array('route' => 'haru_user_group_list'));

        $user = $root->addChild('route.haru_user_user_index', array('route' => 'haru_user_user_index'));
        //$user->addChild('route.haru_user_user_list', array('route' => 'haru_user_user_list'));

        $number = $root->addChild('route.number_number_index', array('route' => 'number_number_index'));
        //$number->addChild('route.number_number_list', array('route' => 'number_number_list'));

        $provider = $root->addChild('route.provider_provider_index', array('route' => 'provider_provider_index'));
        //$provider->addChild('route.provider_provider_list', array('route' => 'provider_provider_list'));

        $rating = $root->addChild('route.rating_rating_index', array('route' => 'rating_rating_index'));
        //$rating->addChild('route.rating_rating_list', array('route' => 'rating_rating_list'));

        // ~

        return $root;
    }

    // ~ Getters and setters.

    /**
     * @return FactoryInterface
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @param FactoryInterface $factory
     * @return Builder
     */
    public function setFactory(FactoryInterface $factory)
    {
        $this->factory = $factory;
        return $this;
    }

}
