<?php

namespace IndexBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * IndexBundle class.
 *
 * @package IndexBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class IndexBundle extends Bundle
{

}
