<?php

namespace ProviderBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ProviderBundle class.
 *
 * @package ProviderBundle
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProviderBundle extends Bundle
{

}
