<?php

namespace ProviderBundle\EventListener;

use Haru\Component\EventDispatcher\EventListenerInterface,
    ProviderBundle\Event\FilterProviderResponseEventInterface,
    ProviderBundle\Event\FormEventInterface,
    ProviderBundle\Event\GetProviderResponseEventInterface;

/**
 * ProviderListenerInterface interface.
 *
 * @package ProviderBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ProviderListenerInterface extends EventListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetProviderResponseEventInterface $event
     */
    public function onListInitialize(GetProviderResponseEventInterface $event);

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    public function onListCompleted(FilterProviderResponseEventInterface $event);

    // ~

    /**
     * @param GetProviderResponseEventInterface $event
     */
    public function onEditInitialize(GetProviderResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    public function onEditCompleted(FilterProviderResponseEventInterface $event);

    // ~

    /**
     * @param GetProviderResponseEventInterface $event
     */
    public function onDeleteInitialize(GetProviderResponseEventInterface $event);

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterProviderResponseEventInterface $event);

}
