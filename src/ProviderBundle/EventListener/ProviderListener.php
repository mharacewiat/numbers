<?php

namespace ProviderBundle\EventListener;

use ProviderBundle\Event\FilterProviderResponseEventInterface;
use ProviderBundle\Event\FormEventInterface;
use ProviderBundle\Event\GetProviderResponseEventInterface;

/**
 * ProviderListener class.
 *
 * @package ProviderBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProviderListener extends ProviderListenerAbstract
{

    // ~ Event listeners.

    /**
     * @param GetProviderResponseEventInterface $event
     */
    public function onListInitialize(GetProviderResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    public function onListCompleted(FilterProviderResponseEventInterface $event)
    {
        return;
    }

    // ~

    /**
     * @param GetProviderResponseEventInterface $event
     */
    public function onEditInitialize(GetProviderResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FormEventInterface $event
     */
    public function onEditSuccess(FormEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    public function onEditCompleted(FilterProviderResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'provider.edit');

        // ~

        return;
    }

    // ~

    /**
     * @param GetProviderResponseEventInterface $event
     */
    public function onDeleteInitialize(GetProviderResponseEventInterface $event)
    {
        return;
    }

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    public function onDeleteCompleted(FilterProviderResponseEventInterface $event)
    {
        $this->getFlashMessenger()->add('success', 'provider.delete');

        // ~

        return;
    }

}
