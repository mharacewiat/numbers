<?php

namespace ProviderBundle\EventListener;

use Haru\Component\EventDispatcher\EventListener,
    ProviderBundle\Event\FilterProviderResponseEventInterface,
    ProviderBundle\Event\FormEventInterface,
    ProviderBundle\Event\GetProviderResponseEventInterface;

/**
 * ProviderListenerAbstract abstract class.
 *
 * @package ProviderBundle\EventListener
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ProviderListenerAbstract extends EventListener implements ProviderListenerInterface
{

    // ~ Event listeners.

    /**
     * @param GetProviderResponseEventInterface $event
     */
    abstract public function onListInitialize(GetProviderResponseEventInterface $event);

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    abstract public function onListCompleted(FilterProviderResponseEventInterface $event);

    // ~

    /**
     * @param GetProviderResponseEventInterface $event
     */
    abstract public function onEditInitialize(GetProviderResponseEventInterface $event);

    /**
     * @param FormEventInterface $event
     */
    abstract public function onEditSuccess(FormEventInterface $event);

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    abstract public function onEditCompleted(FilterProviderResponseEventInterface $event);

    // ~

    /**
     * @param GetProviderResponseEventInterface $event
     */
    abstract public function onDeleteInitialize(GetProviderResponseEventInterface $event);

    /**
     * @param FilterProviderResponseEventInterface $event
     */
    abstract public function onDeleteCompleted(FilterProviderResponseEventInterface $event);

}
