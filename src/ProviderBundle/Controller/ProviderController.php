<?php

namespace ProviderBundle\Controller;

use Doctrine\DBAL\DBALException,
    ProviderBundle\Entity\Provider,
    ProviderBundle\Event\FilterProviderResponseEvent,
    ProviderBundle\Event\FilterProviderResponseEventInterface,
    ProviderBundle\Event\FormEvent,
    ProviderBundle\Event\FormEventInterface,
    ProviderBundle\Event\GetProviderResponseEvent,
    ProviderBundle\Form\ProviderType,
    ProviderBundle\Repository\ProviderRepository,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * ProviderController class.
 *
 * @package ProviderBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProviderController extends Controller
{

    // ~ Actions.

    /**
     * @return RedirectResponse
     */
    public function indexAction()
    {
        /* Do nothing. Just redirect to provider list action. */
        return $this->redirectToRoute('provider_provider_list', array());
    }

    /**
     * @param $page
     * @param $limit
     * @param Request $request
     * @return Response
     */
    public function listAction($page, $limit, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'providers' => null,
        );

        // ~

        /* Prepare services. */

        $providerManager = $this->getProviderManager();
        $eventDispatcher = $this->getEventDispatcher();
        $calculatorService = $this->getCalculatorService();

        // ~

        $event = new GetProviderResponseEvent(null, $request);
        $eventDispatcher->dispatch(GetProviderResponseEvent::LIST_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        /** @var ProviderRepository $providerRepository */
        $providerRepository = $providerManager->getRepository();

        // ~

        $offset = $calculatorService->calculateOffset($page, $limit);

        // ~

        /* Get all providers. */
        $providers = $providerRepository->getAll(null, $offset, $limit);

        // ~

        /* Fill up render data. */

        $renderData['providers'] = $providers;

        // ~

        $response = $this->render('ProviderBundle:Provider:list.html.twig', $renderData);

        // ~

        $event = new FilterProviderResponseEvent(null, $response);
        $eventDispatcher->dispatch(FilterProviderResponseEventInterface::LIST_COMPLETED, $event);

        // ~

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        /* Prepare render data. */

        $renderData = array(
            'form' => null,
            // ~
            'provider' => null,
        );

        // ~

        /* Prepare services. */

        $providerManager = $this->getProviderManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var ProviderRepository $providerRepository */
        $providerRepository = $providerManager->getRepository();

        /** @var Provider $provider */
        $provider = $providerRepository->find($id);

        // ~

        if (true == is_null($provider)) {
            /** @var Provider $provider */
            $provider = $providerManager->create();
        }

        // ~

        $event = new GetProviderResponseEvent($provider, $request);
        $eventDispatcher->dispatch(GetProviderResponseEvent::EDIT_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        $formType = new ProviderType();
        $formOptions = array();

        $form = $this->createForm($formType, $provider, $formOptions);

        // ~

        if (true == $request->isMethod('post')) {
            $form->handleRequest($request);

            // ~

            if (true == $form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch(FormEventInterface::EDIT_SUCCESS, $event);

                // ~

                try {
                    /* Update provider. */
                    $providerManager->update($provider, true);

                    // ~

                    $event = new FilterProviderResponseEvent($provider, $response);
                    $eventDispatcher->dispatch(FilterProviderResponseEventInterface::EDIT_COMPLETED, $event);

                    // ~

                    return $this->redirectToRoute('provider_provider_list', array());
                } catch (DBALException $exception) {
                    $this->addFlash('error', 'edit');
                } catch (\Exception $exception) {
                    $this->addFlash('error', $exception->getMessage());
                }
            }
        }

        // ~

        /* Fill up render data. */

        $renderData['form'] = $form->createView();
        $renderData['provider'] = $provider;

        // ~

        return $this->render('ProviderBundle:Provider:edit.html.twig', $renderData);
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        /* Prepare services. */

        $providerManager = $this->getProviderManager();
        $eventDispatcher = $this->getEventDispatcher();

        // ~

        /** @var ProviderRepository $repository */
        $repository = $providerManager->getRepository();

        /** @var Provider $provider */
        $provider = $repository->find($id);

        // ~

        $event = new GetProviderResponseEvent($provider, $request);
        $eventDispatcher->dispatch(GetProviderResponseEvent::DELETE_INITIALIZE, $event);

        if (false == is_null($response = $event->getResponse())) {
            return $response;
        }

        // ~

        if (false == is_null($provider)) {
            try {
                /* Update provider. */
                $providerManager->delete($provider, true);
            } catch (DBALException $exception) {
                $this->addFlash('error', 'delete');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // ~

        $response = $this->redirectToRoute('provider_provider_list', array());

        // ~

        $event = new FilterProviderResponseEvent($provider, $response);
        $eventDispatcher->dispatch(FilterProviderResponseEventInterface::DELETE_COMPLETED, $event);

        // ~

        return $response;
    }

}
