<?php

namespace ProviderBundle\Controller;

use Haru\Bundle\FrameworkBundle\Controller\Controller as BaseController,
    ProviderBundle\Service\Manager as ProviderManager;

/**
 * Controller class.
 *
 * @package ProviderBundle\Controller
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class Controller extends BaseController
{

    // ~ Getters and setters.

    /**
     * @return ProviderManager
     */
    protected function getProviderManager()
    {
        return $this->getContainer()->get('provider.manager');
    }

}
