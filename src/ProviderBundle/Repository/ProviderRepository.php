<?php

namespace ProviderBundle\Repository;

use Haru\ORM\EntityRepository;

/**
 * ProviderRepository class.
 *
 * @package ProviderBundle\Repository
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class ProviderRepository extends EntityRepository
{

}
