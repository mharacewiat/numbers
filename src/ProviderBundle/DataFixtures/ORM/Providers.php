<?php

namespace ProviderBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager,
    Haru\Common\DataFixture,
    ProviderBundle\Entity\Provider,
    ProviderBundle\Service\Manager as ProviderManager;

/**
 * Providers class.
 *
 * @package ProviderBundle\DataFixtures
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class Providers extends DataFixture
{

    // ~ Main methods.

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* Load providers data. */
        $providersData = $this->loadProvidersData();

        // ~

        /* Load providers. */
        $this->loadProviders($providersData, $manager);

        // ~

        return;
    }

    // ~ Helpers.

    /**
     * @param array $providersData
     * @param ObjectManager $manager
     */
    protected function loadProviders(array $providersData, ObjectManager $manager)
    {
        /* Iterate through providers data. */
        foreach ($providersData as $key => $value) {
            /* Load provider. */
            $this->loadProvider($key, $value, $manager);
        }

        // ~

        /* Flush. */
        $manager->flush();

        // ~

        return;
    }

    /**
     * @param $key
     * @param array $providerData
     * @param ObjectManager $manager
     * @return Provider
     */
    protected function loadProvider($key, array $providerData, ObjectManager $manager)
    {
        /* Build provider. */
        $provider = $this->buildProvider($key, $providerData, $manager);

        // ~

        return $provider;
    }

    // ~

    /**
     * @param $key
     * @param array $providerData
     * @param ObjectManager $manager
     * @return Provider
     */
    protected function buildProvider($key, array $providerData, ObjectManager $manager)
    {
        unset($manager);

        // ~

        $providerManager = $this->getProviderManager();

        // ~

        /* Prepare data. */

        /**
         * @var string $name
         */
        list($name) = $this->prepareData($providerData);

        // ~

        $provider = $providerManager->create();

        // ~

        $provider->setName($name);

        // ~

        $providerManager->update($provider, false);

        // ~

        /* Store provider. */
        $this->storeProvider($key, $provider);

        // ~

        return $provider;
    }

    // ~

    /**
     * @param $key
     * @param Provider $provider
     * @return $this
     */
    protected function storeProvider($key, Provider $provider)
    {
        $this->setReference(vsprintf('provider-%1$s', array($key)), $provider);

        return $this;
    }

    // ~

    /**
     * @return mixed
     */
    protected function loadProvidersData()
    {
        return $this->loadData('providers');
    }

    // ~

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $this->fillData(array('name'), $data);
    }

    // ~ Getters and setters.

    /**
     * @return ProviderManager
     */
    protected function getProviderManager()
    {
        return $this->getContainer()->get('provider.manager');
    }

    // ~

    /**
     * @return int
     */
    public function getOrder()
    {
        return 201;
    }

}
