<?php

namespace ProviderBundle\Entity;

use Haru\ORM\Mapping\EntityInterface;

/**
 * ProviderInterface class.
 *
 * @package ProviderBundle\Entity
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ProviderInterface extends EntityInterface
{


    // ~ Getters and setters.

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set ProviderInterface
     *
     * @param string $name
     *
     * @return ProviderInterface
     */
    public function setName($name);

    /**
     * Get ProviderInterface
     *
     * @return string
     */
    public function getName();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProviderInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProviderInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt();

}
