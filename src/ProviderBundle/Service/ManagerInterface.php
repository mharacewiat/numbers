<?php

namespace ProviderBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository,
    ProviderBundle\Entity\ProviderInterface;

/**
 * ManagerInterface interface.
 *
 * @package ProviderBundle\Service
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ManagerInterface
{

    // ~ Main methods.

    /**
     * @return ProviderInterface
     */
    public function create();

    /**
     * @param ProviderInterface $provider
     * @param bool $flush
     * @return $this
     */
    public function update(ProviderInterface $provider, $flush = true);

    /**
     * @param ProviderInterface $provider
     * @param bool $flush
     * @return $this
     */
    public function delete(ProviderInterface $provider, $flush = true);

    // ~ Getters and setters.

    /**
     * @return ObjectRepository
     */
    public function getRepository();

    // ~

    /**
     * @return string
     */
    public function getClass();

}
