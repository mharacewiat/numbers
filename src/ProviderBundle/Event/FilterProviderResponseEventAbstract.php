<?php

namespace ProviderBundle\Event;

/**
 * FilterProviderResponseEventAbstract abstract class.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class FilterProviderResponseEventAbstract extends ProviderEventAbstract implements FilterProviderResponseEventInterface
{

}
