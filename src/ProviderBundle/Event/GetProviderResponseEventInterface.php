<?php

namespace ProviderBundle\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * GetProviderResponseEventInterface interface.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface GetProviderResponseEventInterface extends ProviderEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_INITIALIZE = 'provider.provider.list.initialize';

    /**
     * @var string
     */
    const EDIT_INITIALIZE = 'provider.provider.edit.initialize';

    /**
     * @var string
     */
    const DELETE_INITIALIZE = 'provider.provider.delete.initialize';

    // ~ Getters and setters.

    /**
     * @return Response|null
     */
    public function getResponse();

}
