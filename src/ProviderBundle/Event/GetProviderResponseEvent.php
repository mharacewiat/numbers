<?php

namespace ProviderBundle\Event;

/**
 * GetProviderResponseEvent class.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class GetProviderResponseEvent extends GetProviderResponseEventAbstract
{

}
