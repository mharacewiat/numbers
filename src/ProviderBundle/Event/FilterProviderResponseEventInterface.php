<?php

namespace ProviderBundle\Event;

/**
 * FilterProviderResponseEventInterface interface.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface FilterProviderResponseEventInterface extends ProviderEventInterface
{

    // ~ Constants.

    /**
     * @var string
     */
    const LIST_COMPLETED = 'provider.provider.list.completed';

    /**
     * @var string
     */
    const EDIT_COMPLETED = 'provider.provider.edit.completed';

    /**
     * @var string
     */
    const DELETE_COMPLETED = 'provider.provider.delete.completed';

}
