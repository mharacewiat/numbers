<?php

namespace ProviderBundle\Event;

/**
 * FilterProviderResponseEvent class.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
class FilterProviderResponseEvent extends FilterProviderResponseEventAbstract
{

}
