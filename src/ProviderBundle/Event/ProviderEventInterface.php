<?php

namespace ProviderBundle\Event;

use Haru\Component\EventDispatcher\EventInterface,
    ProviderBundle\Entity\ProviderInterface;

/**
 * ProviderEventInterface interface.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
interface ProviderEventInterface extends EventInterface
{

    // ~ Getters and setters.

    /**
     * @return ProviderInterface|null
     */
    public function getProvider();

}
