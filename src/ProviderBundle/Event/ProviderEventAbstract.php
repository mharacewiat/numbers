<?php

namespace ProviderBundle\Event;

use Haru\Component\EventDispatcher\Event,
    ProviderBundle\Entity\ProviderInterface;

/**
 * ProviderEventAbstract abstract class.
 *
 * @package ProviderBundle\Event
 * @author Haracewiat <m.haracewiat@gmail.com>
 */
abstract class ProviderEventAbstract extends Event implements ProviderEventInterface
{

    // ~ Properties.

    /**
     * @var ProviderInterface|null
     */
    protected $provider = null;

    // ~ Magic methods.

    /**
     * ProviderEventAbstract constructor.
     *
     * @param ProviderInterface|null $provider
     */
    public function __construct(ProviderInterface $provider = null)
    {
        ///* Construct parent. */
        //parent::__construct();

        // ~

        $this->setProvider($provider);

        // ~

        return;
    }

    // ~ Getters and setters.

    /**
     * @return ProviderInterface|null
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param ProviderInterface|null $provider
     * @return $this
     */
    protected function setProvider(ProviderInterface $provider = null)
    {
        $this->provider = $provider;
        return $this;
    }

}
